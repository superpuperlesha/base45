//===PRELOADER AJAX===
var Loading    = '<div class="d-flex justify-content-center"><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>';
var arrIMGt    = ['', '.jpg', '.jpeg', '.png'];
var csmapstyle = [];
var fadeSpeed  = 250;



/* Load-More */
/* var data = new FormData();
data.append( 'action',      'escrollpage' );
data.append( 'e_list_page', e_list_page );
data.append( 'e_city',      e_city );
fetch( WPajaxURL, {
	method: 'POST',
	body:   data,
}).then(
	response => response.text()
).then(
	html => {
		e_listloadmo.innerHTML     = '';
		e_listloadmo_box.innerHTML += html;
		if(html.length > 0){
			e_list_page_loading = 0;
		}
	}
); */



	/*document.addEventListener("DOMContentLoaded", function(event){
		document.getElementById('xxx_click0').addEventListener('click', (e) => {
			let dd = document.createElement('div');
			dd.innerHTML = 'ttimeddd';
			dd.classList.add('xxx');
			document.getElementById('xxxpar').appendChild( dd )
		}, false);

		document.getElementById('xxx_click').addEventListener('click', (e) => {
			document.getElementById('xxxpar').querySelectorAll('.xxx').forEach(function(el) {
				el.style.color = '#ff0000';
				console.log(el);
			});
		}, false);

		document.getElementById('xxx_click1').addEventListener('click', (e) => {
			var x = document.getElementById('xxxpar').querySelectorAll('div');
			x[0].style.color = '#ff0000';
			x[x.length-1].style.color = '#ff0000';
		}, false);
	});*/

	/*document.addEventListener("DOMContentLoaded", function(event){
		const spmr_gotocheckout = document.getElementById('spmr_gotocheckout');
		const spmr_gotocheckouterrmsg = document.getElementById('spmr_gotocheckouterrmsg');
		
		spmr_gotocheckout.addEventListener('click', function(event){
			const spmr_sellprodid         = document.getElementById('spmr_sellprodid').value;
			const spmr_vidth              = document.getElementById('spmr_vidth').value;
			const spmr_height             = document.getElementById('spmr_height').value;
			spmr_gotocheckouterrmsg.innerHTML = spmr_gotocheckouterrmsg.getAttribute('data-loading');
			
			fetch(WPajaxURL, {
				method:      'POST',
				credentials: 'same-origin',
				headers: {
					'Content-Type':  'application/x-www-form-urlencoded',
					'Cache-Control': 'no-cache',
				},
				body: new URLSearchParams({
					action:           'spmr_gotocheckout',
					spmr_sellprodid:   spmr_sellprodid,
					spmr_vidth:        spmr_vidth,
					spmr_height:       spmr_height,
				})
			}).then(response=>response.json())
			.then(
				response=>{
					spmr_gotocheckouterrmsg.innerHTML = response.msg;
					if(response.err == 0){
						top.location.href = response.redirecturl;
					}
					//console.log(response);
				}
			)
			.catch(err=>console.log(err));
		});
	});*/


//===AJAX===
/*var CSRF_TOKEN       = document.querySelector('meta[name="csrf-token"]').content;
var cds_loadmore_btn = document.getElementById('cds_loadmore_btn');
cds_loadmore_btn.addEventListener('click', function(){
    function XLoad1(){
        var xhttp = new XMLHttpRequest();
        var data = new FormData();
        data.append('_token', CSRF_TOKEN );
        data.append('postvar1', '111' );
        data.append('postvar2', '222' );
        xhttp.onreadystatechange = function(){
            if(this.readyState === XMLHttpRequest.DONE && this.status === 200){
                document.getElementById('dcs_box').innerHTML = this.responseText;
            }
        };
        xhttp.open('POST', HelloControllerURL, true);
        //xhttp.setRequestHeader('Content-Type', 'application/json');
        //xhttp.setRequestHeader('X-CSRF-TOKEN', CSRF_TOKEN);
        xhttp.send(data);
    }
    XLoad1();
});*/



//===reload ajaxpage on goback===
window.addEventListener('popstate', function(event){
	if( event.state=='base_ajaxpage' ){
		top.location.reload();
	}
});
function setURLpage(pURL){
	window.history.pushState('base_ajaxpage', '', pURL);
}
$(document).on('click', '.openAJAX a', function(event){
	event.stopPropagation();
	event.preventDefault();
	var pURL   = $(this).attr('href');
	var pageid = $(this).attr('data-pageid');
	$('#base_main').fadeTo(fadeSpeed, 0, 'linear', function(){  });
	if( Number.isInteger(parseInt(pageid)) ){
		setURLpage(pURL);
		//$('#base_main').html(Loading);
		$.ajax({
			type: 'POST',
			url:  WPajaxURL,
			data:{
					action: 'openAJAX',
					pageid: pageid,
				 },
			success: function(data, textStatus, XMLHttpRequest) {
				$('#base_main').html(data);
				$('#base_main').fadeTo('slow', 1, function(){  });
			},
			error: function(MLHttpRequest, textStatus, errorThrown) {
				top.location.href = WPURL;
			}
		});
	}
});
//===//reload ajaxpage on goback===


//===PAGE EVENTS===
document.addEventListener('DOMContentLoaded', function(){
	//===CHECK NEW EVENTS, PMGS, GMSG===
	if(CheckTimeEvents>0){
		function CheckTimeEventsFN(){
			var getCountGMSG = $('#getCountGMSG').val();
			var eventID      = $('#eventID').val();
			$.ajax({
				type: 'POST',
				url:  WPajaxURL,
				data: {
						action:  'checkevents',
						eventID: eventID,
					  },
				success: function(data, textStatus, XMLHttpRequest) {
					data = JSON.parse(data);
					//siteNotyfy(data.notify);
					//sitePMSG(data.pmsg);
					//siteGMSG(data.gmsg);
					setTimeout(CheckTimeEventsFN, CheckTimeEvents);
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					//top.location.href = WPURL;
				}
			});
		}
		setTimeout(CheckTimeEventsFN, CheckTimeEvents);
	}
});


