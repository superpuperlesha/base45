<?php
$userCount = 0;
$userCount += ( (int)get_post_meta($post->ID, 'sevnt_user_1', true)>0 ?1 :0 );
$userCount += ( (int)get_post_meta($post->ID, 'sevnt_user_2', true)>0 ?1 :0 );
$userCount += ( (int)get_post_meta($post->ID, 'sevnt_user_3', true)>0 ?1 :0 );

$DataTime  = date(get_option('date_format'), (int)get_post_meta($post->ID, 'sevnt_datetime', true));
$Location  = get_post_meta($post->ID, 'sevnt_location_title', true);

$OrgName = get_user_by('id', $post->post_author);
if($OrgName){
	$OrgName = $OrgName->first_name;
}else{
	$OrgName = __('User not found', 'base');
}

$UpPs = ( (int)get_post_meta($post->ID, 'sevnt_datetime', true)>time() ?true :false ); ?>

<div class="row mt-1">
   	<a href="<?= get_permalink() ?>" class="w51p_url_event col-md-3" data-eventid="<?= $post->ID ?>">
   		<img src="<?= get_event_thumb_url($post->ID, 'thumbnail_300x200') ?>" alt="EAva" class="img-fluid" width="300" height="200">
   	</a>
	<div class="col-md-8">
		<h2>
			<a href="<?= get_permalink() ?>" class="w51p_url_event" data-eventid="<?= $post->ID ?>"><?= get_the_title($post->ID) ?></a>
		</h2>
		<ul>
			<li><i class="fa fa-map-marker" aria-hidden="true"></i><?= $Location  ?></li>
			<li><i class="fa fa-calendar"   aria-hidden="true"></i><?= $DataTime  ?></li>
			<li><i class="fa fa-users"      aria-hidden="true"></i><?= $userCount ?></li>
			<li><i class="fa fa-home"       aria-hidden="true"></i><?= $OrgName   ?></li>
		</ul>
	</div>
	<div class="col-md-1">
		<?php if($UpPs){ ?>
			<span class="event-list__label event-list__label--success"><?= __('Upcoming', 'base') ?></span>
		<?php }else{ ?>
			<span class="event-list__label event-list__label--warning"><?= __('Past', 'base') ?></span>
		<?php } ?>
	</div>
</div>