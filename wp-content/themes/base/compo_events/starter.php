<?php

/*===MENU PAGE events===
$id = get_field('sopt_events_pid', 'option')
<li class="w51p_menu_events">
	<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
</li>*/

add_action( 'wp_footer', 'action_function_name_2472' );
function action_function_name_2472(){
	get_template_part('/compo_events/starter_add');
}


//===PAGE EVENTS===
function page_events($pg=1, $past=0){
    //===CHECK PAYMENT===
    $checkUserCS = checkUserCS(true);
    if($checkUserCS){
        echo $checkUserCS;
        exit();
    }

	GLOBAL $post;

	//===VALIDATION===
	$pg = (int)$pg;
	if($pg<1){
		$pg = 1;
	}

	$past = (int)$past;
	if($past<=0){
		$past = 0;
	}

    $sopt_events_pid = get_field('sopt_events_pid', 'option'); ?>

  	<section class="container">
        <div class="row">
            <div class="col-md-12">
              	<h1><?= get_the_title($sopt_events_pid) ?></h1>

                <?= apply_filters('the_content', get_post_field('post_content', $sopt_events_pid)) ?>
              	<button class="btn btn-primary dc_event_create"><?= __('Create a Get Togethers', 'base') ?></button>

	            <span class="w51p_menu_events">
                    <a href="<?= esc_url(get_the_permalink($sopt_events_pid)) ?>"       class="badge badge-secondary <?= ($past ?'' :'badge-success') ?>" data-pg="1"><?= __('Created', 'base') ?></a>
                </span>
                <span class="paging_events_p">
                    <a href="<?= esc_url(get_the_permalink($sopt_events_pid)) ?>/?past" class="badge badge-secondary <?= ($past ?'badge-success' :'') ?>" data-pg="1"><?= __('Invited', 'base') ?></a>
                </span>

                <?php
    		    $eventPr = get_posts(array('posts_per_page' => 52,
    										'post_type'     => 'event',
    										'fields'        => 'ids',
    										'meta_query'    => [
    												[
    													'relation' => 'OR',
    														[
    															'key'     => 'sevnt_user_1',
    															'value'   => get_current_user_id(),
    															'compare' => '=',
    														],
    														[
    															'key'     => 'sevnt_user_2',
    															'value'   => get_current_user_id(),
    															'compare' => '=',
    														],
    														[
    															'key'     => 'sevnt_user_3',
    															'value'   => get_current_user_id(),
    															'compare' => '=',
    														],
    												]
    										]));
    			$eventMy = get_posts(array('posts_per_page'  => 52,
    									   'post_type'       => 'event',
    									   'fields'          => 'ids',
    									   'meta_query'      => [
    											[
    												'relation' => 'AND',
    													[
    														'key'     => 'sevnt_authot',
    														'value'   => get_current_user_id(),
    														'compare' => '=',
    													],
    											]
    									]));

    			if($past==0){
    				$lastposts = &$eventMy;
    			}else{
    				$lastposts = &$eventPr;
    			} ?>

               	<sdiv><?= count($lastposts).' '.__('events found', 'base') ?></div><?php

    			//$new_gmsg = (array)unserialize( get_user_meta(get_current_user_id(), 'new_gmsg', true) );
    			$ppg = get_option('posts_per_page');
    			$ii  = 0;
    			for($i=($pg==1 ?0 :(($pg-1)*$ppg));$i<count($lastposts);$i++){
    				$post = get_post($lastposts[$i]);
    				get_template_part('compo_events/item_event', '', []);
    				if(++$ii>=$ppg){break;}
    			}
    			echo sitePagingAJAX(($past==1 ?'paging_events_p' :'paging_events'), count($lastposts), $pg, $ppg); ?>
            </div>
        </div>
  	</section><?php
}


//===PAGE EVENT===
function page_event($eventid=0, $msg=''){
    //===CHECK PAYMENT===
    $checkUserCS = checkUserCS(true);
    if($checkUserCS){
        echo $checkUserCS;
        exit();
    }

	$eventid = (int)$eventid;
	$post    = get_post($eventid);
	$cur_usr = get_current_user_id(); ?>

    <section class="container">
        <div class="row">
            <div class="col-md-12"><?php
    			if($post){
    				$user_u1 = (int)get_post_meta($post->ID, 'sevnt_user_1', true);
    				$user_u2 = (int)get_post_meta($post->ID, 'sevnt_user_2', true);
    				$user_u3 = (int)get_post_meta($post->ID, 'sevnt_user_3', true);

    				$confirm_u1 = (int)get_post_meta($post->ID, 'sevnt_user_1_y', true);
    				$confirm_u2 = (int)get_post_meta($post->ID, 'sevnt_user_2_y', true);
    				$confirm_u3 = (int)get_post_meta($post->ID, 'sevnt_user_3_y', true);

    				$confirm_my = false;
    				if( $cur_usr == $user_u1 && $confirm_u1 == 1 ){$confirm_my = true;}
    				if( $cur_usr == $user_u2 && $confirm_u2 == 1 ){$confirm_my = true;}
    				if( $cur_usr == $user_u3 && $confirm_u3 == 1 ){$confirm_my = true;}

    				$user_ucount = 0;
    				$user_ucount += ($user_u1>0 ?1 :0);
    				$user_ucount += ($user_u2>0 ?1 :0);
    				$user_ucount += ($user_u3>0 ?1 :0);

    				if( in_array($cur_usr, [(int)$post->post_author, $user_u1, $user_u2, $user_u3]) ){
    					$user_creator = get_user_by('id', $post->post_author);
    					if($user_creator){ ?>
							<img src="<?= get_event_thumb_url($post->ID, 'thumbnail_652x425') ?>" alt="Ava Event" width="652" height="425">
			              	<h1 class="event-sigle__title section__title"><?= get_the_title($post->ID) ?></h1>
			              	<ul>
				                <li>
				                	<i class="fa fa-map-marker" aria-hidden="true"></i>
				                	<?= get_post_meta($post->ID, 'sevnt_location_title', true) ?>
				                </li>
				                <li>
				                	<i class="fa fa-calendar" aria-hidden="true"></i>
				                	<?= date(get_option('date_format'), (int)get_post_meta($post->ID, 'sevnt_datetime', true)) ?>
				                </li>
				                <li>
				                	<i class="fa fa-users" aria-hidden="true"></i>
				                	<?= $user_ucount ?>
				                </li>
				                <li>
				                	<i class="fa fa-home" aria-hidden="true"></i>
				                	<?php if((int)get_post_meta($post->ID, 'sevnt_public_place', true)){
				                		_e('Confirm this is a public place', 'base');
				                	}else{
				                		_e('This is a not public place', 'base');
				                	} ?>
				                </li>
			              	</ul><?php

                            if($user_creator->ID == $cur_usr){ ?>
								<div>
									<button class="btn btn-danger dc_event_edit"    data-eventid="<?= $post->ID ?>"><?= __('Edit', 'base') ?></button>
                                    <button class="btn btn-warning dc_confirm_open" data-eventid="<?= $post->ID ?>"><?= __('Delete', 'base') ?></button>
								</div><?php
							}else{ ?>
								<div>
									<button class="btn <?= ($confirm_my  ?'btn-success' :'btn-secondary') ?> sevnt_user_yn_submitter" data-eventid="<?= $post->ID ?>" data-commit="1"><?= __('I’ll be there', 'base') ?></button>
									<button class="btn <?= (!$confirm_my ?'btn-secondary' :'btn-success') ?> sevnt_user_yn_submitter" data-eventid="<?= $post->ID ?>" data-commit="2"><?= __('I can’t make it', 'base') ?></button>
								</div><?php
                            } ?>


			              	<a href="<?= get_page_uri(get_field('sopt_myprofile_view_pid', 'option')) ?>/?profile=<?= $user_creator->ID ?>" class="event-single__organizer-info__img dc_myacc_view" data-userid="<?= $user_creator->post_author ?>">
			                	<img  id="mainavaacc" src="<?= get_myavatar_url($post->post_author, 'thumbnail_64x64') ?>" width="64" height="64" alt="ava">
			                </a>
			                <span><?= $user_creator->first_name ?></span>
			                <?= get_user_meta($post->post_author, 'usr_bio', true) ?>
			                 
			                <h2><?= __('Get Together details', 'base') ?></h2>
			                <?= apply_filters('the_content', get_the_content('', '', $post->ID)) ?>
    				              
				            <ul>
				                <li>
				                  	<a href="<?= get_page_uri(get_field('sopt_myprofile_view_pid', 'option')) ?>/?profile=<?= $user_creator->ID ?>" class="dc_myacc_view" data-userid="<?= $user_creator->ID ?>">
										<img src="<?= get_myavatar_url($user_creator->ID, 'thumbnail_64x64') ?>" alt="Ava" width="64" height="64">
									</a>
				                  	<strong><?= $user_creator->first_name ?></strong>
				                  	<span>(<?= __('Organizer', 'base') ?>)</span>
				                </li><?php

				                if( $user_u1 > 0 ){
				                	$user_u1 = get_user_by('id', $user_u1);
									if($user_u1){ ?>
										<li>
						                  	<a href="<?= get_page_uri(get_field('sopt_myprofile_view_pid', 'option')) ?>/?profile=<?= $user_u1->ID ?>" class="dc_myacc_view" data-userid="<?= $user_u1->ID ?>">
												<img src="<?= get_myavatar_url($user_u1->ID, 'thumbnail_64x64') ?>" alt="Ava" width="64" height="64">
											</a>
						                  	<strong><?= $user_u1->first_name ?></strong>
						                  	<span><?php
												if( $confirm_u1==0 ){
													_e('(No answer)', 'base');
												}
												if( $confirm_u1==1 ){
													_e('(I’ll be there)', 'base');
												}
												if( $confirm_u1==2 ){
													_e('(I can\'t make it)', 'base');
												} ?>
											</span>
						                </li><?php
									}else{ ?>
										<li><h2><?= __('User not exists!', 'base') ?></h2></li><?php
									}
								}

								if( $user_u2 > 0 ){
									$user_u2 = get_user_by('id', $user_u2);
									if($user_u2){ ?>
										<li>
						                  	<a href="<?= get_page_uri(get_field('sopt_myprofile_view_pid', 'option')) ?>/?profile=<?= $user_u2->ID ?>" class="dc_myacc_view" data-userid="<?= $user_u2->ID ?>">
												<img src="<?= get_myavatar_url($user_u2->ID, 'thumbnail_64x64') ?>" alt="Ava" width="64" height="64">
											</a>
						                  	<strong><?= $user_u2->first_name ?></strong>
						                  	<span><?php
												if( $confirm_u2==0 ){
													_e('(No answer)', 'base');
												}
												if( $confirm_u2==1 ){
													_e('(I’ll be there)', 'base');
												}
												if( $confirm_u2==2 ){
													_e('(I can\'t make it)', 'base');
												} ?>
											</span>
						                </li><?php
									}else{ ?>
										<li><h2><?= __('User not exists!', 'base') ?></h2></li><?php
									}
								}

								if( $user_u3 > 0 ){
									$user_u3 = get_user_by('id', $user_u3);
									if($user_u3){ ?>
										<li>
						                  	<a href="<?= get_page_uri(get_field('sopt_myprofile_view_pid', 'option')) ?>/?profile=<?= $user_u3->ID ?>" class="dc_myacc_view" data-userid="<?= $user_u3->ID ?>">
												<img src="<?= get_myavatar_url($user_u3->ID, 'thumbnail_64x64') ?>" alt="Ava" width="64" height="64">
											</a>
						                  	<strong><?= $user_u3->first_name ?></strong>
						                  	<span><?php
												if( $confirm_u3==0 ){
													_e('(No answer)', 'base');
												}
												if( $confirm_u3==1 ){
													_e('(I’ll be there)', 'base');
												}
												if( $confirm_u3==2 ){
													_e('(I can\'t make it)', 'base');
												} ?>
											</span>
						                </li><?php
									}else{ ?>
										<li><h2><?= __('User not exists!', 'base') ?></h2></li><?php
									}
								} ?>
				            </ul>
    				            
                            <div id="singl_event_map"></div>
							<script>
								var singl_event_title  = '<?= htmlspecialchars(get_the_title($post->ID)) ?>';
								var singl_event_map_lt = <?= (float)get_post_meta($post->ID, 'sevnt_location_lt', true) ?>;
								var singl_event_map_ln = <?= (float)get_post_meta($post->ID, 'sevnt_location_ln', true) ?>;
							</script>

    				        <?= page_gchat($eventid, $msg) ?>

    					<?php }else{ ?>
    						<h2><?= __('The author of the event was not found!', 'base') ?></h2><?php
    					}

    				}else{ ?>
    					<h2><?= __('You are not a participant in this event!', 'base') ?></h2><?php
    				}

    			}else{ ?>
    				<h2><?= __('Event not exists!', 'base') ?></h2><?php
    			} ?>
            </div>
		</div>
    </section><?php
}


function get_event_thumb_url($eID = 0, $size = 'thumbnail_426x276'){
    $img = (int)get_post_meta($eID, 'sevnt_thumb', true);
    $img = wp_get_attachment_image_src($img, $size);
    return (isset($img[0]) ?$img[0] :esc_url(get_template_directory_uri()) . '/img/noava.jpg');
}


/* Register a custom post type called "Event posts". */
function dc_f2_init() {
    $labels = array(
        'name'                  => _x( 'Event posts', 'Post type general name', 'base' ),
        'singular_name'         => _x( 'Event posts', 'Post type singular name', 'base' ),
        'menu_name'             => _x( 'Event posts', 'Admin Menu text', 'base' ),
        'name_admin_bar'        => _x( 'Event posts', 'Add New on Toolbar', 'base' ),
        'add_new'               => __( 'Add New', 'base' ),
        'add_new_item'          => __( 'Add New', 'base' ),
        'new_item'              => __( 'New Event posts', 'base' ),
        'edit_item'             => __( 'Edit Event posts', 'base' ),
        'view_item'             => __( 'View Event posts', 'base' ),
        'all_items'             => __( 'All Event posts', 'base' ),
        'search_items'          => __( 'Search Event posts', 'base' ),
        'parent_item_colon'     => __( 'Parent Event posts:', 'base' ),
        'not_found'             => __( 'No Event posts found.', 'base' ),
        'not_found_in_trash'    => __( 'No Event posts found in Trash.', 'base' ),
        'featured_image'        => _x( 'Cover image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'base' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'base' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'base' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'base' ),
        'archives'              => _x( 'Event posts archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'base' ),
        'insert_into_item'      => _x( 'Insert into Event posts', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'base' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Event posts', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'base' ),
        'filter_items_list'     => _x( 'Filter Event posts list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'base' ),
        'items_list_navigation' => _x( 'Event posts list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'base' ),
        'items_list'            => _x( 'Event posts list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'base' ),
    );
    
    $args = array(
        'labels'             => $labels,
        'description'        => 'Event custom post type.',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug'=>'my-events' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 20,
        'supports'           => array( 'title', 'editor' ), //, 'excerpt', 'comments', 'author', 'thumbnail'
        'taxonomies'         => array(),
        'show_in_rest'       => true
    );
    register_post_type( 'event', $args );
    
}
add_action( 'init', 'dc_f2_init' );


//===CREATE / UPDATE EVENT===
function CRUevent(){
    $err = ['id'=>'', 'txt'=>''];
    $eventID = (int)$_POST['uevent_crt_eventid'];

    if ($eventID > 0) {
        $p = get_post($eventID);
        if (!$p || !in_array(get_current_user_id(), [get_post_meta($eventID, 'sevnt_authot', true)])) {
            return __('You can not update this event!', 'base');;
        }
    }

    $_POST['event_crt_name']        = substr(sanitize_text_field($_POST['event_crt_name']), 0, 2000);
    $_POST['event_crt_content']     = substr(sanitize_text_field($_POST['event_crt_content']), 0, 10000);
    $_POST['event_crt_place_title'] = sanitize_text_field($_POST['event_crt_place_title']);
    $_POST['event_crt_place_ln']    = sanitize_text_field($_POST['event_crt_place_ln']);
    $_POST['event_crt_place_lt']    = sanitize_text_field($_POST['event_crt_place_lt']);
    $_POST['event_crt_publiczone']  = ($_POST['event_crt_publiczone'] === 'on' ? 1 : 0);
    $_POST['event_crt_euser_1']     = (int)$_POST['event_crt_euser_1'];
    $_POST['event_crt_euser_2']     = (int)$_POST['event_crt_euser_2'];
    $_POST['event_crt_euser_3']     = (int)$_POST['event_crt_euser_3'];
    $_POST['event_crt_date_ts']     = (int)$_POST['event_crt_date_ts'];
    $dts = $_POST['event_crt_date_ts'] / 1000;

    if ($_POST['event_crt_date_ts'] < 10) {
        $err['txt'] = __('Please enter event "Date Time".', 'base');
    }
    if (strlen($_POST['event_crt_name']) < 6) {
        $err['txt'] = __('Please enter "Get Together name".', 'base');
    }
    if (strlen($_POST['event_crt_content']) < 60) {
        $err['txt'] = __('Please enter "Message to the invitees".', 'base');
    }
    //if (strlen($_POST['event_crt_place_title']) < 3) {
        //$err['txt'] = __('Enter a more detailed address.', 'base');
    //}
    //if (strlen($_POST['event_crt_place_ln']) < 3) {
        //$err['txt'] = __('Enter a more detailed address.', 'base');
    //}
    //if (strlen($_POST['event_crt_place_lt']) < 3) {
        //$err['txt'] = __('Enter a more detailed address.', 'base');
    //}

    //===CLEAR DUPLICATE USERS===
    if ($_POST['event_crt_euser_1'] == $_POST['event_crt_euser_2']) {
        $_POST['event_crt_euser_2'] = 0;
    }
    if ($_POST['event_crt_euser_1'] == $_POST['event_crt_euser_3']) {
        $_POST['event_crt_euser_3'] = 0;
    }
    if ($_POST['event_crt_euser_2'] == $_POST['event_crt_euser_3']) {
        $_POST['event_crt_euser_3'] = 0;
    }

    if ($err['txt'] == '') {
        if ($eventID == 0) {
            $post_data = array(
                'post_title' => $_POST['event_crt_name'],
                'post_content' => $_POST['event_crt_content'],
                'post_status' => 'publish',
                'post_type' => 'event',
            );
            $eventID = wp_insert_post($post_data);

            //===LOG BLOCK usr_cf_3===
            if ($_POST['event_crt_euser_1'] > 0) {
                if ((int)get_user_meta($_POST['event_crt_euser_1'], 'usr_cf_3', true)) {
                    setLogEmail($_POST['event_crt_euser_1'], __('You are invited to a new meeting.', 'base'));
                }
                setLog($_POST['event_crt_euser_1'], __('You are invited to a new meeting.', 'base'));
            }
            if ($_POST['event_crt_euser_2'] > 0) {
                if ((int)get_user_meta($_POST['event_crt_euser_2'], 'usr_cf_3', true)) {
                    setLogEmail($_POST['event_crt_euser_2'], __('You are invited to a new meeting.', 'base'));
                }
                setLog($_POST['event_crt_euser_2'], __('You are invited to a new meeting.', 'base'));
            }
            if ($_POST['event_crt_euser_3'] > 0) {
                if ((int)get_user_meta($_POST['event_crt_euser_3'], 'usr_cf_3', true)) {
                    setLogEmail($_POST['event_crt_euser_3'], __('You are invited to a new meeting.', 'base'));
                }
                setLog($_POST['event_crt_euser_3'], __('You are invited to a new meeting.', 'base'));
            }
            //===============
        } else {
            $post_data = array(
                'ID' => $eventID,
                'post_title' => $_POST['event_crt_name'],
                'post_content' => $_POST['event_crt_content'],
            );
            wp_update_post($post_data);

            //===LOG BLOCK usr_cf_4===
            if ($_POST['event_crt_euser_1'] > 0) {
                if ((int)get_user_meta($_POST['event_crt_euser_1'], 'usr_cf_4', true)) {
                    setLogEmail($_POST['event_crt_euser_1'], __('The meeting was changed.', 'base'));
                }
                setLog($_POST['event_crt_euser_1'], __('The meeting was changed.', 'base'));
            }
            if ($_POST['event_crt_euser_2'] > 0) {
                if ((int)get_user_meta($_POST['event_crt_euser_2'], 'usr_cf_4', true)) {
                    setLogEmail($_POST['event_crt_euser_2'], __('The meeting was changed.', 'base'));
                }
                setLog($_POST['event_crt_euser_2'], __('The meeting was changed.', 'base'));
            }
            if ($_POST['event_crt_euser_3'] > 0) {
                if ((int)get_user_meta($_POST['event_crt_euser_3'], 'usr_cf_4', true)) {
                    setLogEmail($_POST['event_crt_euser_3'], __('The meeting was changed.', 'base'));
                }
                setLog($_POST['event_crt_euser_3'], __('The meeting was changed.', 'base'));
            }
            //===============
        }

        //===add PHOTO===
        if (isset($_FILES['sevnt_thumb'])) {
            if (isset($_FILES['sevnt_thumb']) && isset($_POST['fileup_nonce']) && count($_FILES['sevnt_thumb']) > 0 && $_FILES['sevnt_thumb']['tmp_name'] && wp_verify_nonce($_POST['fileup_nonce'], 'sevnt_thumb')) {
                $file = $_FILES['sevnt_thumb'];
                require_once(ABSPATH . 'wp-admin/includes/admin.php');
                $file_return = wp_handle_upload($file, array('test_form' => false));
                if (isset($file_return['error']) || isset($file_return['upload_error_handler'])) {
                    $err['txt'] .= __('Error uploading photo!', 'base');
                }else{
                    $filename = $file_return['file'];
                    $attachment = array(
                        'post_mime_type' => $file_return['type'],
                        'post_title'     => htmlspecialchars(basename($filename)),
                        'post_content'   => '',
                        'post_status'    => 'inherit',
                        'guid'           => $file_return['url'],
                    );
                    $attachment_id = wp_insert_attachment($attachment, $file_return['url']);
                    require_once(ABSPATH . 'wp-admin/includes/image.php');
                    $attachment_data = wp_generate_attachment_metadata($attachment_id, $filename);
                    wp_update_attachment_metadata($attachment_id, $attachment_data);
                    update_post_meta($eventID, 'sevnt_thumb', $attachment_id);
                }
            }
        }

        update_post_meta($eventID, 'sevnt_datetime',       $dts);
        update_post_meta($eventID, 'sevnt_location_title', $_POST['event_crt_place_title']);
        update_post_meta($eventID, 'sevnt_location_ln',    $_POST['event_crt_place_ln']);
        update_post_meta($eventID, 'sevnt_location_lt',    $_POST['event_crt_place_lt']);
        update_post_meta($eventID, 'sevnt_user_1',         $_POST['event_crt_euser_1']);
        update_post_meta($eventID, 'sevnt_user_2',         $_POST['event_crt_euser_2']);
        update_post_meta($eventID, 'sevnt_user_3',         $_POST['event_crt_euser_3']);
        update_post_meta($eventID, 'sevnt_public_place',   $_POST['event_crt_publiczone']);
        update_post_meta($eventID, 'sevnt_authot',         get_current_user_id());

        $err['id'] = $eventID;
    }
    return json_encode($err);
}


//===GETTING FORM EVENT CRU===
function the_form_cru_event($eventID = 0, $formID){
    $eventID = (int)$eventID;
    if ($eventID > 0) {
        $post = get_post($eventID);
        if(!$post){
            _e('Event not exists!', 'base');
            return;
        }
        if(!in_array(get_current_user_id(), [get_post_meta($eventID, 'sevnt_authot', true)])){
            _e('You can not update this event!', 'base');
            return;
        }
    }

    $event_crt_name        = '';
    $event_crt_content     = '';
    $event_crt_date        = '';
    $event_crt_date_ts     = '';
    $event_crt_place_title = '';
    $event_crt_place_ln    = '';
    $event_crt_place_lt    = '';
    $event_crt_publiczone  = '';
    $event_crt_euser_1     = '';
    $event_crt_euser_2     = '';
    $event_crt_euser_3     = '';
    $arr = (array)unserialize(get_user_meta(get_current_user_id(), 'my_favorites', true));

    if ($eventID > 0) {
        $post = get_post($eventID);
        if ($post) {
            $dts                   = (int)get_post_meta($post->ID, 'sevnt_datetime', true);
            $event_crt_name        = $post->post_title;
            $event_crt_content     = $post->post_content;
            $event_crt_date        = date('d.m.Y H:i', $dts);
            $event_crt_date_ts     = $dts * 1000;
            $event_crt_place_title = get_post_meta($post->ID,        'sevnt_location_title', true);
            $event_crt_place_ln    = (float)get_post_meta($post->ID, 'sevnt_location_ln',    true);
            $event_crt_place_lt    = (float)get_post_meta($post->ID, 'sevnt_location_lt',    true);
            $event_crt_publiczone  = (int)get_post_meta($post->ID,   'sevnt_public_place',   true);
            $event_crt_euser_1     = (int)get_post_meta($post->ID,   'sevnt_user_1',         true);
            $event_crt_euser_2     = (int)get_post_meta($post->ID,   'sevnt_user_2',         true);
            $event_crt_euser_3     = (int)get_post_meta($post->ID,   'sevnt_user_3',         true);
        }
    } ?>
    <form action="#uID" method="POST" id="<?= $formID ?>" class="form" enctype="multipart/form-data">
        <div class="form-group">
            <input type="text" id="event_crt_name" name="event_crt_name" value="<?= $event_crt_name ?>" class="ds_focusmy form-control" maxlength="2000">
            <label for="event_crt_name"><?= __('Get Together name', 'base') ?></label>
        </div>
        <div class="form-group">
            <textarea id="event_crt_content" name="event_crt_content" maxlength="10000" class="form-control"><?= $event_crt_content ?></textarea>
            <label for="event_crt_content"><?= __('Add a message to the invitees...', 'base') ?></label>
            <span><?= __('Minimum 60 characters.', 'base') ?></span>
        </div>
        <div class="form-group">
            <input type="text" id="event_crt_date" name="event_crt_date" value="<?= $event_crt_date ?>" data-language="en" data-position="top left" readonly="readonly" class="form-control">
            <label for="event_crt_date"><?= __('Date and time', 'base') ?></label>
            <input type="hidden" id="event_crt_date_ts" name="event_crt_date_ts" value="<?= $event_crt_date_ts ?>">
        </div>
        <div class="form-group">
            <input type="text" id="event_crt_place" name="event_crt_place" value="<?= $event_crt_place_title ?>" class="form-control">
            <label for="event_crt_place"><?= __('Place', 'base') ?></label>
            <input type="hidden" id="event_crt_place_title" name="event_crt_place_title" value="<?= $event_crt_place_title ?>">
            <input type="hidden" id="event_crt_place_ln"    name="event_crt_place_ln" value="<?= $event_crt_place_ln ?>">
            <input type="hidden" id="event_crt_place_lt"    name="event_crt_place_lt" value="<?= $event_crt_place_lt ?>">
            <div id="event_crt_place_title_text"></div>
        </div>
        <div class="form-group">
            <input type="checkbox" id="event_crt_publiczone" name="event_crt_publiczone" <?= ($event_crt_publiczone == 1 ? 'checked' : '') ?> >
            <label for="event_crt_publiczone"><?= __('Confirm this is a public place', 'base') ?></label>
        </div>
        <div class="form-group">
            <select id="event_crt_euser_1" name="event_crt_euser_1" class="form-control"><?php
                echo '<option value="0">' . __('Select user from you favorites', 'base') . '</option>';
                foreach ($arr as $arri) {
                    $user = get_user_by('id', $arri);
                    if ($user) {
                        echo '<option value="' . $user->ID . '" ' . ($user->ID == $event_crt_euser_1 ? 'selected' : '') . '>' . $user->first_name . '</option>';
                    }
                } ?>
            </select>
        </div>
        <div class="form-group">
            <select id="event_crt_euser_2" name="event_crt_euser_2" class="form-control"><?php
                echo '<option value="0">' . __('Select user from you favorites', 'base') . '</option>';
                foreach ($arr as $arri) {
                    $user = get_user_by('id', $arri);
                    if ($user) {
                        echo '<option value="' . $user->ID . '" ' . ($user->ID == $event_crt_euser_2 ? 'selected' : '') . '>' . $user->first_name . '</option>';
                    }
                } ?>
            </select>
        </div>
        <div class="form-group">
            <select id="event_crt_euser_3" name="event_crt_euser_3" class="form-control"><?php
                echo '<option value="0">' . __('Select user from you favorites', 'base') . '</option>';
                foreach ($arr as $arri) {
                    $user = get_user_by('id', $arri);
                    if ($user) {
                        echo '<option value="' . $user->ID . '" ' . ($user->ID == $event_crt_euser_3 ? 'selected' : '') . '>' . $user->first_name . '</option>';
                    }
                } ?>
            </select>
        </div>
        <div class="form-group">
            <input type="hidden" name="uevent_crt_eventid" value="<?= $eventID ?>">
            <?= __('Photo', 'base') ?>
            <?php wp_nonce_field('sevnt_thumb', 'fileup_nonce') ?>
            <span><?= __('Choose a file', 'base') ?></span>
            <input type="file" id="sevnt_thumb" name="sevnt_thumb" accept=".jpg, .jpeg, .png" data-fsize="<?= __('Image no more 2mB.', 'base') ?>" data-ftype="<?= __('Please enter image from (.jpg, .jpeg, .png).', 'base') ?>">
        </div>
    </form><?php
}


//===ACCEPT EVENT===
add_action('wp_ajax_yngotoevent',        'yngotoevent');
//add_action('wp_ajax_nopriv_yngotoevent', 'yngotoevent');
function yngotoevent(){
	$_POST['eventid']    = (int)$_POST['eventid'];
	$_POST['evenaccept'] = (int)$_POST['evenaccept'];

	if( (int)get_post_meta($_POST['eventid'], 'sevnt_user_1', true) == get_current_user_id() ){
		update_post_meta($_POST['eventid'], 'sevnt_user_1_y', $_POST['evenaccept']);
	}
	if( (int)get_post_meta($_POST['eventid'], 'sevnt_user_2', true) == get_current_user_id() ){
		update_post_meta($_POST['eventid'], 'sevnt_user_2_y', $_POST['evenaccept']);
	}
	if( (int)get_post_meta($_POST['eventid'], 'sevnt_user_3', true) == get_current_user_id() ){
		update_post_meta($_POST['eventid'], 'sevnt_user_3_y', $_POST['evenaccept']);
	}

	page_event($_POST['eventid']);
	
	die();
}

//===SAVE FORM EDIT EVENT===
add_action('wp_ajax_updateevent',        'updateevent');
//add_action('wp_ajax_nopriv_updateevent', 'updateevent');
function updateevent(){
	echo CRUevent();
	die();
}

//===CREATE FORM EVENT===
add_action('wp_ajax_createevent',        'createevent');
//add_action('wp_ajax_nopriv_createevent', 'createevent');
function createevent(){
	echo CRUevent();
	die();
}

//===GET FORM EDIT/CREATE EVENT===
add_action('wp_ajax_getformeditevent',        'getformeditevent');
//add_action('wp_ajax_nopriv_getformeditevent', 'getformeditevent');
function getformeditevent(){
	$_POST['eventid'] = (int)$_POST['eventid'];
	the_form_cru_event($_POST['eventid'], 'event_edit_form');
	die();
}


//===PAGE EVENTS===
add_action('wp_ajax_openevent',        'openevent');
//add_action('wp_ajax_nopriv_openevent', 'openevent');
function openevent(){
	$_POST['eventid'] = $_POST['eventid'] ?? '';
	page_event($_POST['eventid']);
	die();
}

//===PAGE EVENT===
add_action('wp_ajax_menuevents',        'menuevents');
//add_action('wp_ajax_nopriv_menuevents', 'menuevents');
function menuevents(){
	$_POST['pg']   = $_POST['pg']   ?? '';
	$_POST['past'] = $_POST['past'] ?? '';
	page_events($_POST['pg'], $_POST['past']);
	die();
}


//===LOAD CLEAR FORM FOR ADDING ACTION===
add_action('wp_ajax_loadcleareventform',        'loadcleareventform');
//add_action('wp_ajax_nopriv_loadcleareventform', 'loadcleareventform');
function loadcleareventform(){
	the_form_cru_event(0, 'event_crt_form');
	die();
}

//===DELETE EVENT===
add_action('wp_ajax_deleteevent',        'deleteevent');
//add_action('wp_ajax_nopriv_deleteevent', 'deleteevent');
function deleteevent(){
	$eventid = $_POST['eventid'] ?? 0;

	$p = get_post($eventid);
	if( $p ){
		if( get_current_user_id() == $p->post_author ){
			wp_delete_post( $eventid, true );
			page_events(1, 0);
		}else{
			_e('You cannot delete this event!', 'base');
		}
	}else{
		_e('The event does not exist!', 'base');
	}
	
	die();
} ?>