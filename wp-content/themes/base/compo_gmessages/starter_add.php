<script>
	
	//jQuery(document).ready(function($){
	document.addEventListener('DOMContentLoaded', function(){
		
		//===REPLY TO gmsg SETUP===
		$(document).on('click', '.gmsg_reply', function(){
			var parentID = $(this).attr('data-parmesid');
			$('#gmsg_reply_parent_id').val(parentID);
			$('#gmsg_reply_parent_title').html('Reply to group message: '+parentID);
			$('#gmsg_reply_cancel').removeClass('d-none').addClass('d-inline');
		});

		//===DELETE GROUP MESSAGE===
		$(document).on('click', '.gmsg_deleter', function(){
			var msgid       = $(this).attr('data-msgid');
			var profileuser = $(this).attr('data-profileuser');

			$('#base_main').fadeTo(fadeSpeed, 0, 'linear', function(){  });
			$.ajax({
				type: 'POST',
				url:  WPajaxURL,
				data: {
						action:      'delgmsg',
						profileuser: profileuser,
						msgid:       msgid,
					  },
				success: function(data, textStatus, XMLHttpRequest) {
					$('#base_main').html(data);
					$('#base_main').fadeTo('slow', 1, function(){  });
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});
		});

		//===MENU OPEN GMSG===
		$(document).on('click', '.w51p_menu_gmsgs', function(){
			var pageid = $(this).attr('data-pageid');
			event.stopPropagation();
			event.preventDefault();
			setURLpage($(this).find('a').attr('href'));
			$('#base_main').fadeTo(fadeSpeed, 0, 'linear', function(){  });
			$.ajax({
				type: 'POST',
				url:  WPajaxURL,
				data: {
						action:'getgmsg',
						pageid: pageid,
					  },
				success: function(data, textStatus, XMLHttpRequest) {
					$('#base_main').html(data);
					$('#base_main').fadeTo('slow', 1, function(){  });
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});
		});

		//===SEND GROUP MESSAGE===
		$(document).on('click', '#gmsg_submit_post', function(event){
			event.preventDefault();
			var err                  = '';
			var profileuser          = $('#gmsg_profileuser').val();
			var gmsg_title           = $('#gmsg_title').val();
			var gmsg_txt             = $('#gmsg_txt').val();
			var gmsg_reply_parent_id = $('#gmsg_reply_parent_id').val();
			var gmsg_eid             = $('#gmsg_eid').val();
			var IDListing2           = $('#gmsg_IDListing2').val();

			if(gmsg_txt.length<36){
				err = $(this).attr('data-errmsg');
			}
			if(err==''){
				$('#base_main').fadeTo(fadeSpeed, 0, 'linear', function(){  });
				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data: {
							action:               'writegmsg',
							profileuser:          profileuser,
							gmsg_title:           gmsg_title,
							gmsg_txt:             gmsg_txt,
							gmsg_reply_parent_id: gmsg_reply_parent_id,
							gmsg_eid:             gmsg_eid,
							IDListing2:           IDListing2
						  },
					success:     function(data, textStatus, XMLHttpRequest) {
						$('#base_main').html(data);
						$('#base_main').fadeTo('slow', 1, function(){  });
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
			}else{
				$('#gmsg_notation_box').html(err);
			}
		});

		//===RELOAD GMSG===
		function siteGMSG(GmsgCount=0){
			if($('#getCountGMSG').length && $('#eventID').length && $('#gmsg_title').length && $('#gmsg_txt').length){
				var getCountGMSG = parseInt($('#getCountGMSG').val());
				var eventID      = parseInt($('#eventID').val());
				var GmsgCount    = parseInt(GmsgCount);
				if(getCountGMSG != GmsgCount){
					//openEvent(eventID, $('#gmsg_title').val(), $('#gmsg_txt').val(), true);
				}
			}
		}

	});
</script>