<?php
/*
Template Name: Page [group chat]
*/ ?>

<?php if(!is_user_logged_in()){
	wp_redirect(home_url().'/?upenauth');
	exit();
} ?>

<?php get_header() ?>

<?php page_gchat(get_queried_object_id()) ?>

<?php get_footer() ?>