<?php

add_image_size('thumbnail_64x64', 64, 64, true);


define('threadFilled',  esc_url(get_template_directory_uri()).'/compo_threads_pchats/img/star_1.svg');
define('threadNFilled', esc_url(get_template_directory_uri()).'/compo_threads_pchats/img/star_2.svg');


/*===MENU PAGE PMESSAGES===
$id = get_field('sopt_messages_pid', 'option') ?>
<li class="menu_pchats <?= (getPMsgUnReadedCount(get_current_user_id())>0    ?'active' :'') ?>">
    <a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
</li>*/
//===PAGE PCHATS===
add_action('wp_ajax_menupchats',        'menupchats');
//add_action('wp_ajax_nopriv_menupchats', 'menupchats');
function menupchats(){
    page_pchats();
    die();
}


add_action('wp_footer', 'action_function_name_2475');
function action_function_name_2475(){
    get_template_part('/compo_threads_pchats/starter_add');
}


/*//===Check new private-message===
add_action('cmb2_admin_init', 'custom_metabox');
function custom_metabox(){
    $cmb = new_cmb2_box([
        'id'           => 'sopt_pchat',
        'title'        => 'Private messages setup',
        'object_types' => ['page'],
        'show_on'      => ['key'=>'page-template', 'value'=>'compo_threads_pchats/a_page_myaccount_messages.php'],
    ]);
    $cmb->add_field([
        'name'    => __('Check new messages [sec]', 'base'),
        'desc'    => '',
        'default' => '10',
        'id'      => 'sopt_ajax_check_sec',
        'type'    => 'text',
    ]);
}*/


//===private chat new messages count===
function getPMsgUnReadedCount(){
    return count((array)unserialize(get_user_meta(get_current_user_id(), 'new_pmsg', true)));
}


//===Y/Y thread in favorites===
function get_yn_thread_fav($userID){
    if (array_search((int)$userID, (array)unserialize(get_user_meta(get_current_user_id(), 'my_thread_fav', true))) === false){
        return false;
    }else{
        return true;
    }
}


function GETpmsgfortSent($postID){
    GLOBAL $current_user;
    $res      = '';
    $post     = get_post($postID);
    $touserID = 0;

    if($post){
        $terms = get_the_terms($post->ID, 'strpchat');
        if(is_array($terms)){
            foreach($terms as $term){
                $s_t = (int)get_term_meta($term->term_id, 's_t', true);
                if($s_t){
                    $touserID = $s_t;
                }
            }
        }

        $user = get_user_by('id', $touserID);
        //if($user){
            $res .='<div>
                        <a href="'.get_permalink(get_field('sopt_myprofile_view_pid', 'option')).'/?profile='.$touserID.'" class="dc_myacc_view" data-userid="'.$touserID.'">
                            <img src="'.get_myavatar_url($touserID, 'thumbnail_64x64').'" alt="Avatar" width="64" height="64">
                        </a>
                        <div>'.__('To', 'base').': '.(isset($user->first_name) ?$user->first_name :__('User not found!', 'base')).'</div>
                        <time>'.get_the_time(get_option('date_format'), $post->ID).'</time>
                        <h4>'.$post->post_title.'</h4>
                        '.$post->post_content.'
                    </div>';
        //}
    }
    return $res;
}


function deleteThread($threadid=0){
    GLOBAL $current_user;
    $threadid = (int)$threadid;
    $s_f      = get_term_meta($threadid, 's_f', true);
    $s_t      = get_term_meta($threadid, 's_t', true);
    
    if( $s_f==$current_user->ID || $s_t==$current_user->ID ){
        $posts = get_posts([
              'posts_per_page' => -1,
              'post_type'      => 'pmsg',
              'fields'         => 'ids',
              'tax_query'      => [
                  [
                      'taxonomy' => 'strpchat',
                      'field'    => 'term_id',
                      'terms'    => $threadid,
                  ]
              ]
        ]);
        foreach($posts as $postid){
            wp_delete_post($postid);
        }
        wp_delete_term($threadid, 'strpchat');

        //===delete thread from favorite===
        $arr = (array)unserialize(get_user_meta($s_f, 'my_thread_fav', true));
        unset($arr[array_search($threadid, $arr)]);
        $arr = array_values($arr);
        update_user_meta($s_f, 'my_thread_fav', serialize($arr));

        //===delete thread from favorite===
        $arr = (array)unserialize(get_user_meta($s_t, 'my_thread_fav', true));
        unset($arr[array_search($threadid, $arr)]);
        $arr = array_values($arr);
        update_user_meta($s_t, 'my_thread_fav', serialize($arr));
    }
}


//===write pmsg===
function write_pmsg($strimID = 0, $userID = 0, $pmsg_title = '', $pmsg_txt = '', $pmsg_reply_parent_id = 0){
    $res                  = '';
    $strimID              = (int)$strimID;
    $userID               = (int)$userID;
    $pmsg_reply_parent_id = (int)$pmsg_reply_parent_id;
    $pmsg_title           = substr(sanitize_text_field($pmsg_title), 0, 1000);
    $pmsg_txt             = substr(sanitize_text_field($pmsg_txt),   0, 5000);

    if($userID < 1){
        $userID = 0;
    }

    if(strlen($pmsg_title) < 3){
        $res = __('Please enter title.', 'base');
    }

    if(strlen($pmsg_txt) < 36){
        $res = __('Please enter Content.', 'base');
    }

    //===insert message===
    if(!$res){
        $post_id = wp_insert_post([
            'post_title'   => $pmsg_title,
            'post_content' => $pmsg_txt,
            'post_status'  => 'publish',
            'post_author'  => get_current_user_id(),
            'post_type'    => 'pmsg',
            'post_parent'  => (int)$pmsg_reply_parent_id,
        ]);

        //===IF NOT DRAFT===
        if($userID>0 && $strimID>0){
            wp_set_post_terms($post_id, [$strimID], 'strpchat');

            $arr = (array)unserialize(get_user_meta($userID, 'new_pmsg', true));
            $arr[$strimID] = 1;
            update_user_meta($userID, 'new_pmsg', serialize($arr));

            if((int)get_user_meta($userID, 'usr_cf_1', true)){
                setLogEmail($userID, __('You received a private message.', 'base'));
            }
            setLog($userID, __('You received a private message.', 'base'));
        }
    }

    return $res;
}


//===I READ all messages to this THREAD===
function new_pmsg_read($threadid=0){
    $threadid = (int)$threadid;
    $arr = (array)unserialize(get_user_meta(get_current_user_id(), 'new_pmsg', true));
    unset($arr[(int)$threadid]);
    update_user_meta(get_current_user_id(), 'new_pmsg', serialize($arr));
}


//===I UNREAD all messages to this THREAD===
function new_pmsg_uread($threadid=0){
    $threadid = (int)$threadid;
    $arr = (array)unserialize(get_user_meta(get_current_user_id(), 'new_pmsg', true));
    if(!array_key_exists($threadid, $arr)){
        $arr[(int)$threadid] = 1;
    }
    update_user_meta(get_current_user_id(), 'new_pmsg', serialize($arr));
}


//===add THREADS TO fav===
add_action('wp_ajax_favsthreads',        'favsthreads');
//add_action('wp_ajax_nopriv_favsthreads', 'favsthreads');
function favsthreads(){
    $res                = '';
    $_POST['threadarr'] = $_POST['threadarr'] ?? [];
    $_POST['threadarr'] = (array)$_POST['threadarr'];
    if( count($_POST['threadarr']) ){
        $arr = (array)unserialize(get_user_meta(get_current_user_id(), 'my_thread_fav', true));
        foreach( $_POST['threadarr'] as $threadarri ){
            if(array_search($threadarri, $arr) === false){
                $arr[] = $threadarri;
            }
        }
        update_user_meta(get_current_user_id(), 'my_thread_fav', serialize($arr));
    }
    theThreadList(__('Thread set to favorite!', 'base'));
    die();
}


//===add THREADS TO UNREAD===
add_action('wp_ajax_unreadthreads',        'unreadthreads');
//add_action('wp_ajax_nopriv_unreadthreads', 'unreadthreads');
function unreadthreads(){
    $res                = '';
    $_POST['threadarr'] = $_POST['threadarr'] ?? [];
    $_POST['threadarr'] = (array)$_POST['threadarr'];
    if( count($_POST['threadarr']) ){
        $arr = (array)unserialize(get_user_meta(get_current_user_id(), 'my_thread_fav', true));
        foreach( $_POST['threadarr'] as $threadarri ){
            new_pmsg_uread($threadarri);
        }
        update_user_meta(get_current_user_id(), 'my_thread_fav', serialize($arr));
    }
    theThreadList(__('Thread set as unread!', 'base'));
    die();
}


//===add THREADS TO UNREAD===
add_action('wp_ajax_readthreads',        'readthreads');
//add_action('wp_ajax_nopriv_readthreads', 'readthreads');
function readthreads(){
    $res                = '';
    $_POST['threadarr'] = $_POST['threadarr'] ?? [];
    $_POST['threadarr'] = (array)$_POST['threadarr'];
    if( count($_POST['threadarr']) ){
        $arr = (array)unserialize(get_user_meta(get_current_user_id(), 'my_thread_fav', true));
        foreach( $_POST['threadarr'] as $threadarri ){
            new_pmsg_read($threadarri);
        }
        update_user_meta(get_current_user_id(), 'my_thread_fav', serialize($arr));
    }
    theThreadList(__('Thread set as readed!', 'base'));
    die();
}


//===dell THREAD===
add_action('wp_ajax_delthreads',        'delthreads');
//add_action('wp_ajax_nopriv_delthreads', 'delthreads');
function delthreads(){
    $res                = '';
    $_POST['threadarr'] = $_POST['threadarr'] ?? [];
    $_POST['threadarr'] = (array)$_POST['threadarr'];
    if( count($_POST['threadarr']) ){
        foreach( $_POST['threadarr'] as $threadarri ){
            deleteThread($threadarri);
        }
    }
    page_pchats(__('Thread deleted!', 'base'));
    die();
}


//===add/dell THREAD TO fav===
add_action('wp_ajax_threadfav',        'threadfav');
//add_action('wp_ajax_nopriv_threadfav', 'threadfav');
function threadfav(){
    $res   = 0;
    $favid = $_POST['threadid'] ?? (int)$_POST['threadid'] ?? 0;
    $arr   = (array)unserialize(get_user_meta(get_current_user_id(), 'my_thread_fav', true));
    if(array_search($favid, $arr) !== false){
        unset($arr[array_search($favid, $arr)]);
        $arr = array_values($arr);
        $res = 0;
    }else{
        $arr[] = $favid;
        $res = 1;
    }
    update_user_meta(get_current_user_id(), 'my_thread_fav', serialize($arr));
    echo $res;
    die();
}


//===list pmsg for thread===
function GETpmsgforthread($threadid=0, $msg=''){
    GLOBAL $current_user;

    //===VALIDATION===
    $threadid = (int)$threadid;
    if($threadid<1){
        $threadid = 0;
    }

    new_pmsg_read($threadid);

    $s_f = get_term_meta($threadid, 's_f', true);
    $s_t = get_term_meta($threadid, 's_t', true);
    if($s_f==$current_user->ID){
        $avaUSRID = $s_t;
    }else{
        $avaUSRID = $s_f;
    }

    $res = '<div class="row">
                <div class="col-md-12">
                    <div class="text-danger">'.htmlspecialchars($msg).'</div>
                    <h3>'.__('Message subject', 'base').'</h3>';
    
    $arr = (array)unserialize(get_user_meta(get_current_user_id(), 'usr_blocked_pmsg', true));
    if( !in_array(get_current_user_id(), array_keys($arr)) ){
        $res .= '<form action="#uID" method="POST">
        			<div class="form-group">
	                    <div      id="pmsg_notation_box"       class="text-danger"></div>
	                    <span     id="pmsg_reply_parent_title" class="text-success"></span>
	                    <span     id="pmsg_reply_cancel"       class="fa fa-times text-success d-none" title="'.__('Cancel reply message!', 'base').'"></span>
	                    <input    id="pmsg_reply_parent_id"    name="pmsg_reply_parent_id" type="hidden">
	                </div>
                    <div class="form-group">
                        <input    id="pmsg_title" name="pmsg_title" type="text" class="form-control" data-errmsg="'.__('Please enter Title', 'base').'" maxlength="1000" placeholder="'.__('Subject', 'base').'">
                    </div>
                    <div class="form-group">
                        <textarea id="pmsg_txt"   name="pmsg_txt" maxlength="5000" class="form-control" data-errmsg="'.__('Please enter Message', 'base').'" placeholder="'.__('Write your message...', 'base').'"></textarea>
                    </div>
                    <div class="form-group">
                    	<input    id="pmsg_threadid"           name="pmsg_threadid"    type="hidden" value="'.$threadid.'">
	                    <input    id="pmsg_userid"             name="pmsg_userid"      type="hidden" value="'.$avaUSRID.'">
                        <button id="pmsg_submit_post" class="btn btn-primary" type="submit">' . __('Send', 'base') . '</button>
                    </div>
                </form>';
    }else{
        $res .= '<p class="text-danger">'.__('You cannot write to this private user!', 'base').'</p>';
    }
    
    //===getting all private messages===
    $postsall = get_posts([
                    'numberposts'      => 51,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'pmsg',
                    'fields'           => '',
                    'tax_query' => [
                                        'relation' => 'AND',
                                        [
                                            'taxonomy' => 'strpchat',
                                            'field'    => 'id',
                                            'terms'    => [$threadid],
                                            'operator' => 'IN',
                                        ]
                                    ]
                ]);
    foreach( $postsall as $postpar ){
        if($postpar->post_parent==0){
            $userpostdata = get_user_by('id', $postpar->post_author);
            $res .= '<div class="row">
                        <a href="'.get_page_uri(get_field('sopt_myprofile_view_pid', 'option')).'/?profile='.$postpar->post_author.'" class="col-md-2 dc_myacc_view" data-userid="'.$postpar->post_author.'">
                            <img src="'.get_myavatar_url($postpar->post_author).'" class="img-fluid" alt="Ava" width="64" height="64">
                        </a>
                         <div class="col-md-8">
                            <h4>'.($userpostdata->ID==get_current_user_id() ?__('You: ', 'base') :$userpostdata->first_name).''.get_the_title($postpar->ID).'</h4>
                            <time>'.get_the_time(get_option('date_format'), $postpar->ID).'</time>
                            '.apply_filters('the_content', get_post_field('post_content', $postpar->ID)).'
                        </div>
                        <div class="col-md-2">
                            '.($postpar->post_author == get_current_user_id() ?'<img src="'.get_template_directory_uri().'/img/icon-delete.svg"     class="pmsg_deleter" data-msgid="'.$postpar->ID.'" data-threadid="'.$threadid.'" alt="'.__('Delete private message!', 'base').'">':'')
                             .($postpar->post_author != get_current_user_id() ?'<img src="'.get_template_directory_uri().'/img/icon-arrow-left.svg" class="pmsg_reply"   data-parmesid="'.$postpar->ID.'" alt="arrow" width="24" height="20">' :'').'
                        </div>
                    </div>';
            
            foreach( $postsall as $postdoch ){
                if($postdoch->post_parent==$postpar->ID){
                    $userpostdata = get_user_by('id', $postdoch->post_author);
                    $res .= '<div class="row">
                                <a href="'.get_page_uri(get_field('sopt_myprofile_view_pid', 'option')).'/?profile='.$postdoch->post_author.'" class="col-md-2 dc_myacc_view" data-userid="'.$postdoch->post_author.'">
                                    <img src="'.get_myavatar_url($postdoch->post_author).'" alt="Ava" width="64" height="64">
                                </a>
                                <div class="col-md-8">
                                    <h4>'.($userpostdata->ID==get_current_user_id() ?__('You: ', 'base') :$userpostdata->first_name).get_the_title($postdoch->ID).'</h4>
                                    <time>'.get_the_time(get_option('date_format'), $postdoch->ID).'</time>
                                    '.apply_filters('the_content', get_post_field('post_content', $postdoch->ID)).'
                                </div>
                                <div class="col-md-2">
                                    '.($postdoch->post_author == get_current_user_id() ?'<img src="'.get_template_directory_uri().'/img/icon-delete.svg" class="pmsg_deleter" data-msgid="'.$postpar->ID.'" data-threadid="'.$threadid.'" alt="Delete">' :'').'
                                </div>
                            </div>';
                }
            }
        }
    }
    $res .= '</div>';
	return $res;
}


function getDraftList($msg=''){
    GLOBAL $current_user;
    $res = '';
    
    $res .='<div class="row">
                <div class="col-md-12">
                    <div class="text-danger">'.htmlspecialchars($msg).'</div>
                </div>
                <label class="col-md-12" for="dc_thread_selall_3">
                    <input type="checkbox" id="dc_thread_selall_3">
                    '.__('Select all', 'base').'
                    <button id="wd_pmsg_dgft_id_deller" class="btn btn-primary" data-errmsg="'.__('Select messages!', 'base').'">'.__('Delete', 'base').'</button></li>
                </label>
            </div>';
    
    //===getting all private messages===
    $posts = get_posts([
                    'numberposts'      => 51,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'pmsg',
                    'fields'           => 'ids',
                    'author'           => get_current_user_id(),
                    'tax_query' => [
                                        'relation' => 'AND',
                                        [
                                            'taxonomy' => 'strpchat',
                                            'field'    => 'id',
                                            'operator' => 'NOT EXISTS',
                                        ]
                                    ]
                ]);
    $i = 0;
    foreach( $posts as $postsid ){
        $res .= '<div class="row wd_pmsg_draft '.($i++ == 0 ?'bg-secondary' :'').'" data-msgid="'.$postsid.'">
                    <label class="col-md-1" for="mesgdraft_'.$postsid.'">
                        <input type="checkbox" id="mesgdraft_'.$postsid.'" class="wd_pmsg_drft_id w51p_stop_e" value="'.$postsid.'">
                    </label>
                    <div class="col-md-4">
                        <time>'.get_the_time(get_option('date_format'), $postsid).'</time>
                    </div>
                    <div class="col-md-6">
                        <span>'.substr(get_the_title($postsid), 0, 10).'</span>
                        <p>'.substr(strip_tags(get_the_excerpt($postsid)), 0, 20).'</p>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-danger pmsg_deletedraft" data-msgid="'.$postsid.'">X</button>
                    </div>
                </div>';
    }
    return $res;
}


function theThreadList($msg=''){
    GLOBAL $current_user;
    $openStreamID = 0;

    echo'<div>
            <div class="col-md-12 text-success">
                '.htmlspecialchars($msg).'
            </div>
            <div class="col-md-12">
                <label for="dc_thread_selall_1">
                    <input type="checkbox" id="dc_thread_selall_1">
                    '.__('Select all', 'base').'
                </label>
                <button class="btn btn-primary" id="wd_pmsg_thread_id_read"   data-errmsg="'.__('Select thread!', 'base').'">'.__('Mark as read',   'base').'</button>
                <button class="btn btn-primary" id="wd_pmsg_thread_id_unread" data-errmsg="'.__('Select thread!', 'base').'">'.__('Mark as unread', 'base').'</button>
                <button class="btn btn-primary" id="wd_pmsg_thread_id_favser" data-errmsg="'.__('Select thread!', 'base').'">'.__('Make favourite', 'base').'</button>
                <button class="btn btn-primary" id="wd_pmsg_thread_id_deller" data-errmsg="'.__('Select thread!', 'base').'">'.__('Delete', 'base').'</button>
            </div>
        </div>';

    $terms = get_terms([
        'taxonomy'      => ['strpchat'],
        'orderby'       => 'id',
        'order'         => 'DSC',
        'hide_empty'    => false,
        'object_ids'    => null,
        'include'       => [],
        'exclude'       => [],
        'exclude_tree'  => [],
        'number'        => 51,
        'fields'        => 'ids',
        'count'         => false,
        'slug'          => '',
        'parent'        => 0,
        'hierarchical'  => true,
        'child_of'      => '',
        'get'           => '',
        'name__like'    => '',
        'pad_counts'    => false,
        'offset'        => '',
        'search'        => '',
        'cache_domain'  => 'core',
        'name'          => '',
        'childless'     => false,
        'update_term_meta_cache' => true,
        'meta_query'    => [
            'relation' => 'OR',
            [
                'key'     => 's_f',
                'value'   => $current_user->ID,
                'compare' => '=',
                'type'    => 'NUMERIC',
            ],
            [
                'key'     => 's_t',
                'value'   => $current_user->ID,
                'compare' => '=',
                'type'    => 'NUMERIC',
            ],
        ]
    ]);

    
    $arrUnreaded = (array)unserialize( get_user_meta(get_current_user_id(), 'new_pmsg', true) );
    //===clear deleted threads===
    $intsec = array_diff_key($arrUnreaded, $terms);
    if($intsec){
        foreach($intsec as $key=>$intseci){
            unset($arrUnreaded[$key]);
        }
        update_user_meta(get_current_user_id(), 'new_pmsg', serialize($arrUnreaded));
        $arrUnreaded = (array)unserialize( get_user_meta(get_current_user_id(), 'new_pmsg', true) );
    }


    if($terms){
        $i=0;
        foreach($terms as $term){
            $term = get_term_by('ID', $term, 'strpchat', ARRAY_A);
            if($i++==0){
                $openStreamID = $term['term_id'];
            }
            
            $s_f = get_term_meta($term['term_id'], 's_f', true);
            $s_t = get_term_meta($term['term_id'], 's_t', true);
            if($s_f==$current_user->ID){
                $toUserID = $s_t;
            }else{
                $toUserID = $s_f;
            }

            $lastPost = getLastPMSGfromThRead($term['term_id']);
            $user     = get_user_by('id', $toUserID);
            echo'<div id="thr_has_npm_'.$term['term_id'].'" class="row wd_pmsg_thread'.($i==1 ?' bg-secondary' :'').(isset($arrUnreaded[$term['term_id']]) ?' threadhasnewpmsg' :'').'" data-threadid="'.$term['term_id'].'">
                    <label class="col-md-1" for="mesg_thr_'.$term['term_id'].'" data-thread="'.$term['term_id'].'">
                        <input type="checkbox" id="mesg_thr_'.$term['term_id'].'" class="wd_pmsg_thread_id w51p_stop_e" value="'.$term['term_id'].'">
                    </label>
                    <div class="col-md-2">
                        <img src="'.get_myavatar_url($toUserID, 'thumbnail_64x64').'" alt="Avatar" width="64" height="64">
                    </div>
                    <div class="col-md-8">
                        <strong>'.(isset($user->first_name) ?$user->first_name :__('User not found!', 'base')).'</strong>
                        <time>'.(isset($lastPost->ID) ?get_the_time(get_option('date_format'), $lastPost->ID) :'').'</time>
                        <span>'.substr(get_the_title($lastPost), 0, 10).'...</span>
                        <p>'.(isset($lastPost->ID) ?substr(strip_tags(get_post_field('post_content', $lastPost)), 0, 20) :'').'...</p>
                    </div>
                    <div class="col-md-1">
                        <img src="'.(get_yn_thread_fav($term['term_id']) ?threadFilled :threadNFilled).'" id="thr_threadfav_'.$term['term_id'].'" class="dc_threadfav_del" data-thread="'.$term['term_id'].'" alt="fav">
                    </div>
                </div>';
        }
    }else{
        echo'<div class="row">
                <div class="col-md-12">
                    <strong class="messages__item-empty">'.__('No threads!', 'base').'</strong>
                </div>
            </div>';
    }
    return $openStreamID;
}


function getLastPMSGfromThRead($threadID){
	    $res      = false;
	    $threadID = (int)$threadID;
	    $posts    = get_posts([
	                    'numberposts'      => 1,
	                    'orderby'          => 'date',
	                    'order'            => 'DESC',
	                    'post_type'        => 'pmsg',
	                    'fields'           => 'ids',
	                    'tax_query' => [
	                                        'relation' => 'AND',
	                                        [
	                                            'taxonomy' => 'strpchat',
	                                            'field'    => 'id',
	                                            'terms'    => [$threadID],
	                                            'operator' => 'IN',
	                                        ]
	                                    ]
	                ]);
	    foreach( $posts as $postid ){
	        $res = get_post($postid);
	    }
	    return $res;
}


//===LIST PMSG SENT===
function GETpmsgLast($msg=''){
    $res = '';
    GLOBAL $current_user;
    
    if($msg){
        $res .= '<div class="text-danger">'.$msg.'</div>';
    }

    $res .= '<div class="row">
                <label class="col-md-12" for="dc_thread_selall_2">
                    <input type="checkbox" id="dc_thread_selall_2">
                    '.__('Select all', 'base').'
                    <button id="wd_pmsg_msg_id_deller" class="btn btn-primary" data-errmsg="'.__('Select your message!', 'base').'">'.__('Delete', 'base').'</button>
                </label>
            </div>';

    //===getting all private messages===
    $posts = get_posts([
                'numberposts'      => 51,
                'orderby'          => 'date',
                'order'            => 'DESC',
                'post_type'        => 'pmsg',
                'fields'           => 'ids',
                'author'           => get_current_user_id(),
                'tax_query'        => [
                                            'relation' => 'AND',
                                            [
                                                'taxonomy' => 'strpchat',
                                                'field'    => 'id',
                                                'operator' => 'EXISTS',
                                            ]
                                        ]
            ]);
    $i = 0;
    foreach($posts as $postid){
        $i++;
        $touserID  = 0;
        $terms     = get_the_terms($postid, 'strpchat');
        if(is_array($terms)){
            foreach($terms as $term){
                $s_t = (int)get_term_meta($term->term_id, 's_t', true);
                if($s_t){
                    $touserID = $s_t;
                }
            }
        }
        
        $touser = get_user_by('id', $touserID);
        $res .= '<div class="row '.($i==1 ?'bg-secondary' :'').' wd_pmsg_sentet" data-msgid="'.$postid.'">
                    <label class="col-md-1" for="msgsent_'.$postid.'">
                        <input type="checkbox" id="msgsent_'.$postid.'" class="wd_pmsg_sentet_id w51p_stop_e" value="'.$postid.'">
                    </label>
                    <div class="col-md-2">
                        <img src="'.(isset($touser->ID) ?get_myavatar_url($touser->ID) :get_template_directory_uri().'/img/noava.jpg').'" alt="Ava" width="64" height="64">
                    </div>
                    <div class="col-md-8">
                        <strong>'.__('To', 'base').': '.(isset($touser->ID) ?$touser->first_name :__('User not found!', 'base')).'</strong>
                        <time>'.get_the_time(get_option('date_format'), $postid).'</time>
                        <span>'.substr(get_the_title($postid), 0, 10).'</span>
                        <p>'.substr(strip_tags(apply_filters('the_content', get_post_field('post_content', $postid))), 0, 20).'...</p>
                    </div>
                    <div class="col-md-1">
                        <img src="'.get_template_directory_uri().'/img/icon-delete.svg" class="pmsg_deleterst" data-msgid="'.$postid.'" alt="delete">
                    </div>
                </div>';
    }
    
    return $res;
}


//===PAGE PCHATS===
function page_pchats(){
    //===CHECK PAYMENT===
    $checkUserCS = checkUserCS(true);
    if($checkUserCS){
        echo $checkUserCS;
        exit();
    }

    global $current_user; ?>
    <section class="container">
        <div class="row">
            <div class="col-md-5">
                <h1><?= get_the_title(get_field('sopt_messages_pid', 'option')) ?></h1>
                <button class="btn btn-primary wd_pmsg_initlist"><?= __('Write a message', 'base') ?></button>
                <ul class="nav nav-tabs" id="myTabpmsg" role="tablist">
                    <li class="nav-item" id="wd_pmsg_tab1" role="presentation">
                        <a class="nav-link active" id="wd_pmsg_ajax_body_tab1-tab" data-toggle="tab" href="#wd_pmsg_ajax_body_tab1" role="tab" aria-controls="wd_pmsg_ajax_body_tab1" aria-selected="true"><?= __('Contacts', 'base') ?></a>
                    </li>
                    <li class="nav-item" id="wd_pmsg_tab2" role="presentation">
                        <a class="nav-link" id="wd_pmsg_ajax_body_tab2-tab"        data-toggle="tab" href="#wd_pmsg_ajax_body_tab2" role="tab" aria-controls="wd_pmsg_ajax_body_tab2" aria-selected="false"><?= __('Sent', 'base') ?></a>
                    </li>
                    <li class="nav-item" id="wd_pmsg_tab3" role="presentation">
                        <a class="nav-link" id="wd_pmsg_ajax_body_tab3-tab"        data-toggle="tab" href="#wd_pmsg_ajax_body_tab3" role="tab" aria-controls="wd_pmsg_ajax_body_tab3" aria-selected="false"><?= __('Drafts', 'base') ?></a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContentPMSG">
                    <div class="tab-pane fade show active" id="wd_pmsg_ajax_body_tab1" role="tabpanel" aria-labelledby="wd_pmsg_ajax_body_tab1-tab">
                        <?php $FirstStreamID = theThreadList() ?>
                    </div>
                    <div class="tab-pane fade" id="wd_pmsg_ajax_body_tab2"             role="tabpanel" aria-labelledby="wd_pmsg_ajax_body_tab2-tab">
                        tab2...
                    </div>
                    <div class="tab-pane fade" id="wd_pmsg_ajax_body_tab3"             role="tabpanel" aria-labelledby="wd_pmsg_ajax_body_tab3-tab">
                        <?= getDraftList() ?>
                    </div>
                </div>
            </div>
            <div id="wd_pmsg_ajax_body" class="col-md-7">
                <?= ($FirstStreamID ?GETpmsgforthread($FirstStreamID) :__('There are no messages', 'base')) ?>
            </div>
        </div>
    </section><?php
}


function GETpmsgfortDraft($postID){
    GLOBAL $current_user;
    $res      = '';
    $post     = get_post($postID);

    if($post){
        $res .='<div class="row">
                    <div class="col-md-12">
                        <h4>'.__('Draft', 'base').'</h4>
                    </div>
                </div>
                <form action="#uID" method="POST" class="row">
                	<div class="col-md-6">
		                <div class="form-group">
		                    <input type="text"   id="dcrth_user_search" maxlength="51" placeholder="'.__('Search user', 'base').'">
		                </div>
		            </div>
		            <div class="col-md-6">
                     	<div id="dcrth_user_search_box"></div>
		            </div>
		            <div class="col-md-12">
		                <div class="form-group">
		                    <input type="text" id="dcrth_title" class="form-control"   placeholder="'.__('Title message', 'base').'"   maxlength="1000" data-err="'.__('Please enter Title.', 'base').'" value="'.get_the_title($post->ID).'" >
		                </div>
		            </div>
		            <div class="col-md-12">
		                <div class="form-group">
		                    <textarea id="dcrth_content"        class="form-control"   placeholder="'.__('Content message', 'base').'" maxlength="5000" data-err="'.__('Please enter Content.', 'base').'" >'.get_post_field('post_content', $post->ID).'</textarea>
		                </div>
		            </div>
		            <div class="col-md-12">
		                <div id="dcrth_err" class="text-danger"></div>
		                <input type="hidden" id="dcrth_user_id"     data-err="'.__('Please select user!', 'base').'">
		                <input type="hidden" id="dcrth_parent_id"   value="0">
		                <button type="button" id="dcrth_submitter_thrmsg" class="btn btn-primary">'.__('Send', 'base').'</button>
		            </div>
                </form>';
    }
    return $res;
}




//===get sented msg===
add_action('wp_ajax_deleterst',        'deleterst');
//add_action('wp_ajax_nopriv_deleterst', 'deleterst');
function deleterst(){
	$res            = '';
	$_POST['msgid'] = $_POST['msgid'] ?? 0;
	$_POST['msgid'] = (int)$_POST['msgid'];

	$post = get_post((int)$_POST['msgid']);
	if($post){
		if($post->post_author == get_current_user_id()){
			wp_delete_post($post->ID);
			$res = __('Private message deleted!', 'base');
		}else{
			$res = __('You are no author this private message!', 'base');
		}
	}else{
		$res = __('Private message not exists!', 'base');
	}

	echo GETpmsgLast($res);
	die();
}


//===get draft msg===
add_action('wp_ajax_deletedraftid',        'deletedraftid');
//add_action('wp_ajax_nopriv_deletedraftid', 'deletedraftid');
function deletedraftid(){
	$res            = '';
	$_POST['msgid'] = $_POST['msgid'] ?? 0;
	$_POST['msgid'] = (int)$_POST['msgid'];

	$post = get_post((int)$_POST['msgid']);
	if($post){
		if($post->post_author == get_current_user_id()){
			wp_delete_post($post->ID);
			$res = __('Draft message deleted!', 'base');
		}else{
			$res = __('You are no author this draft message!', 'base');
		}
	}else{
		$res = __('Draft message not exists!', 'base');
	}

	echo getDraftList($res);
	die();
}


//===get sented msges arr===
add_action('wp_ajax_deleterstarr',        'deleterstarr');
//add_action('wp_ajax_nopriv_deleterstarr', 'deleterstarr');
function deleterstarr(){
	$res             = '';
	$_POST['msgarr'] = $_POST['msgarr'] ?? '[]';
	$_POST['msgarr'] = (array)$_POST['msgarr'];

	foreach($_POST['msgarr'] as $msgid){
		$post = get_post((int)$msgid);
		if($post){
			if($post->post_author == get_current_user_id()){
				wp_delete_post($post->ID);
				$res = __('Private message deleted!', 'base');
			}else{
				$res = __('You are no author this private message!', 'base');
			}
		}else{
			$res = __('Private message not exists!', 'base');
		}
	}
	
	echo GETpmsgLast($res);
	die();
}


//===get sented msges arr===
add_action('wp_ajax_deledraftarr',        'deledraftarr');
//add_action('wp_ajax_nopriv_deledraftarr', 'deledraftarr');
function deledraftarr(){
	$res             = '';
	$_POST['msgarr'] = $_POST['msgarr'] ?? '[]';
	$_POST['msgarr'] = (array)$_POST['msgarr'];

	foreach($_POST['msgarr'] as $msgid){
		$post = get_post((int)$msgid);
		if($post){
			if($post->post_author == get_current_user_id()){
				wp_delete_post($post->ID);
				$res = __('Draft message deleted!', 'base');
			}else{
				$res = __('You are no author this draft message!', 'base');
			}
		}else{
			$res = __('Draft message not exists!', 'base');
		}
	}
	
	echo getDraftList($res);
	die();
}


//===get sented msgs===
add_action('wp_ajax_pmsgseltlist',        'pmsgseltlist');
//add_action('wp_ajax_nopriv_pmsgseltlist', 'pmsgseltlist');
function pmsgseltlist(){
	echo GETpmsgLast();
	die();
}


//===PMSG SEARCH USERS===
add_action('wp_ajax_intmsglsearchusr',        'intmsglsearchusr');
//add_action('wp_ajax_nopriv_intmsglsearchusr', 'intmsglsearchusr');
function intmsglsearchusr(){
	$_POST['username'] = $_POST['username'] ?? '';
	$_POST['username'] = sanitize_text_field($_POST['username']);
	
	if( strlen($_POST['username']) > 2 ){
		$search_string = esc_attr( trim( get_query_var('s') ) );
		$users = new WP_User_Query( array(
		    'meta_key'     => 'first_name',
		    'meta_value'   => $_POST['username'],
		    'meta_compare' => 'LIKE',
		) );
		$users = $users->get_results();
		foreach($users as $usersi){ ?>
			<div class="search-box__item">
                <input type="radio" name="crth_user_id_list" id="user_id_<?= $usersi->ID ?>" class="crth_user_id_list" value="<?= $usersi->ID ?>">
                <label for="user_id_<?= $usersi->ID ?>">
                    <img src="<?= get_myavatar_url($usersi->ID, 'thumbnail_64x64') ?>" alt="ava" width="32" height="32">
                    <span><?= $usersi->first_name ?></span>
                </label>
			</div><?php
		}
	}else{
		_e('Please enter "First name".', 'base');
	}
	
	die();
}


//===PMSG CREATE THREAD&&pmsg===
add_action('wp_ajax_intmsglcreate',        'intmsglcreate');
//add_action('wp_ajax_nopriv_intmsglcreate', 'intmsglcreate');
function intmsglcreate(){
	$res = '';

	$_POST['userid']   = $_POST['userid'] ?? 0;
	$_POST['userid']   = (int)$_POST['userid'];

	$_POST['parentid'] = $_POST['parentid'] ?? 0;
	$_POST['parentid'] = (int)$_POST['parentid'];

	$_POST['title']    = $_POST['title'] ?? '';

	$_POST['content']  = $_POST['content'] ?? '';

	if($_POST['userid'] < 1){
		$res = __('Please select user!', 'base');
	}

	if(strlen($res) == 0){
		$term = wp_create_term(time(), 'strpchat');
		if( is_array($term) ){
			add_term_meta($term['term_id'], 's_f', get_current_user_id());
			add_term_meta($term['term_id'], 's_t', $_POST['userid']);
			$res = write_pmsg($term['term_id'], $_POST['userid'], $_POST['title'], $_POST['content'], $_POST['parentid']);
		}else{
			$res = __('Error creating stream!', 'base');
		}
	}
	
	die($res);
}


//===PMSG CREATE DRAFT pmsg===
add_action('wp_ajax_draftcreate',        'draftcreate');
//add_action('wp_ajax_nopriv_draftcreate', 'draftcreate');
function draftcreate(){
	$err = '';

	$_POST['title']   = $_POST['title'] ?? '';
	$_POST['title']   = substr(sanitize_text_field($_POST['title']), 0, 200);

	$_POST['content'] = $_POST['content'] ?? '';
	$_POST['content'] = substr(sanitize_text_field($_POST['content']), 0, 600);
	
	$res = write_pmsg(0, 0, $_POST['title'], $_POST['content'], 0);

	echo getDraftList($res);

	echo $res;
	die();
}


//===PMSG CREATE DRAFT pmsg===
add_action('wp_ajax_pmsgforthread',        'pmsgforthread');
//add_action('wp_ajax_nopriv_pmsgforthread', 'pmsgforthread');
function pmsgforthread(){
	$_POST['threadid'] = $_POST['threadid'] ?? 0;	
	$_POST['threadid'] = (int)$_POST['threadid'];
	echo GETpmsgforthread($_POST['threadid'], '');
	die();
}


//===PMSG open sentet===
add_action('wp_ajax_pmsgforsentet',        'pmsgforsentet');
//add_action('wp_ajax_nopriv_pmsgforsentet', 'pmsgforsentet');
function pmsgforsentet(){
	$_POST['msgid'] = $_POST['msgid'] ?? 0;	
	$_POST['msgid'] = (int)$_POST['msgid'];
	echo GETpmsgfortSent($_POST['msgid']);
	die();
}


//===PMSG open draft===
add_action('wp_ajax_pmsgfordraft',        'pmsgfordraft');
//add_action('wp_ajax_nopriv_pmsgfordraft', 'pmsgfordraft');
function pmsgfordraft(){
	$_POST['msgid'] = $_POST['msgid'] ?? 0;	
	$_POST['msgid'] = (int)$_POST['msgid'];
	echo GETpmsgfortDraft($_POST['msgid']);
	die();
}


//===add/dell privat message===
add_action('wp_ajax_blockpmsgadddel',        'blockpmsgadddel');
//add_action('wp_ajax_nopriv_blockpmsgadddel', 'blockpmsgadddel');
function blockpmsgadddel(){
    $myid = $_POST['myid']  ?? (int)$_POST['myid']  ?? 0;
    $myid = $_POST['usrid'] ?? (int)$_POST['usrid'] ?? 0;
    $arr = (array)unserialize(get_user_meta($_POST['usrid'], 'usr_blocked_pmsg', true));
    if(array_key_exists($_POST['myid'], $arr)){
        unset($arr[$_POST['myid']]);
        $res=0;
    }else{
        $arr[$_POST['myid']] = '';
        $res=1;
    }
    update_user_meta($_POST['usrid'], 'usr_blocked_pmsg', serialize($arr));
    echo $res;
    die();
}


/* Register a custom post type called "Message private posts". */
function dc_f4_init() {
    $labels = array(
        'name'                  => _x( 'Message private posts', 'Post type general name', 'base' ),
        'singular_name'         => _x( 'Message private posts', 'Post type singular name', 'base' ),
        'menu_name'             => _x( 'Message private posts', 'Admin Menu text', 'base' ),
        'name_admin_bar'        => _x( 'Message private posts', 'Add New on Toolbar', 'base' ),
        'add_new'               => __( 'Add New', 'base' ),
        'add_new_item'          => __( 'Add New', 'base' ),
        'new_item'              => __( 'New Message private posts', 'base' ),
        'edit_item'             => __( 'Edit Message private posts', 'base' ),
        'view_item'             => __( 'View Message private posts', 'base' ),
        'all_items'             => __( 'Message private posts', 'base' ),
        'search_items'          => __( 'Search Message private posts', 'base' ),
        'parent_item_colon'     => __( 'Parent Message private posts:', 'base' ),
        'not_found'             => __( 'No Message private posts found.', 'base' ),
        'not_found_in_trash'    => __( 'No Message private posts found in Trash.', 'base' ),
        'featured_image'        => _x( 'Cover image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'base' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'base' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'base' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'base' ),
        'archives'              => _x( 'Message private posts archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'base' ),
        'insert_into_item'      => _x( 'Insert into Message private posts', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'base' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Message private posts', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'base' ),
        'filter_items_list'     => _x( 'Filter Message private posts list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'base' ),
        'items_list_navigation' => _x( 'Message private posts list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'base' ),
        'items_list'            => _x( 'Message private posts list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'base' ),
    );
    
    $args = array(
        'labels'             => $labels,
        'description'        => 'Message private custom post type.',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => false,
        'query_var'          => true,
        'rewrite'            => array( 'slug'=>'pmsg' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 20,
        'supports'           => array( 'title', 'editor' ), //, 'excerpt', 'comments', 'author', 'thumbnail'
        'taxonomies'         => array(),
        'show_in_rest'       => true
    );
    register_post_type( 'pmsg', $args );

    $labels = array(
        'name'                       => _x( 'Chat streams', 'Chat Category General Name', 'base' ),
        'singular_name'              => _x( 'Chat streams', 'Chat Category Singular Name', 'base' ),
        'menu_name'                  => __( 'Chat streams', 'base' ),
        'all_items'                  => __( 'All Items', 'base' ),
        'parent_item'                => __( 'Parent Item', 'base' ),
        'parent_item_colon'          => __( 'Parent Item:', 'base' ),
        'new_item_name'              => __( 'New Item Name', 'base' ),
        'add_new_item'               => __( 'Add New Item', 'base' ),
        'edit_item'                  => __( 'Edit Item', 'base' ),
        'update_item'                => __( 'Update Item', 'base' ),
        'view_item'                  => __( 'View Item', 'base' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'base' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'base' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'base' ),
        'popular_items'              => __( 'Popular Items', 'base' ),
        'search_items'               => __( 'Search Items', 'base' ),
        'not_found'                  => __( 'Not Found', 'base' ),
        'no_terms'                   => __( 'No items', 'base' ),
        'items_list'                 => __( 'Items list', 'base' ),
        'items_list_navigation'      => __( 'Items list navigation', 'base' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => true,
        'show_in_menu'               => false,
        'rewrite'                    => array('slug'=>'strpchat', 'with_front'=>false),
    );
    register_taxonomy('strpchat', array('pmsg'), $args);
    
}
add_action( 'init', 'dc_f4_init' );


//===DELETE private mssage===
add_action('wp_ajax_delpmsg',        'delpmsg');
//add_action('wp_ajax_nopriv_delpmsg', 'delpmsg');
function delpmsg(){
    $ErrMSG = '';
    
    $_POST['threadid'] = $_POST['threadid'] ?? '';
    $_POST['threadid'] = (int)$_POST['threadid'];

    $_POST['msgid']    = $_POST['msgid']    ?? '';
    $_POST['msgid']    = (int)$_POST['msgid'];

    $post = get_post($_POST['msgid']);
    if($post){
        if($post->post_author == get_current_user_id()){
            wp_delete_post($post->ID);
            $ErrMSG = __('Message deleted!', 'base');
        }else{
            $ErrMSG = __('You are no author this Message!', 'base');
        }
    }else{
        $ErrMSG = __('Message not exists!', 'base');
    }

    die(GETpmsgforthread($_POST['threadid'], $ErrMSG));
}

//===PAGE private chat===
add_action('wp_ajax_pchatopen',        'pchatopen');
//add_action('wp_ajax_nopriv_pchatopen', 'pchatopen');
function pchatopen(){
    $_POST['profileuser'] = $_POST['profileuser'] ?? '';
    page_pchat($_POST['profileuser'], '');
    die();
}


//===write pmsg && out all===
add_action('wp_ajax_writepmsg',        'writepmsg');
//add_action('wp_ajax_nopriv_writepmsg', 'writepmsg');
function writepmsg(){
    $_POST['pmsg_threadid']        = $_POST['pmsg_threadid']        ?? 0;
    $_POST['pmsg_userid']          = $_POST['pmsg_userid']          ?? 0;
    $_POST['pmsg_title']           = $_POST['pmsg_title']           ?? '';
    $_POST['pmsg_txt']             = $_POST['pmsg_txt']             ?? '';
    $_POST['pmsg_reply_parent_id'] = $_POST['pmsg_reply_parent_id'] ?? 0;
    
    $res = write_pmsg($_POST['pmsg_threadid'], $_POST['pmsg_userid'], $_POST['pmsg_title'], $_POST['pmsg_txt'], $_POST['pmsg_reply_parent_id']);
    die( GETpmsgforthread($_POST['pmsg_threadid'], $res) );
}


//===CHECK NEW "notifys, pmsgs, gmsgs"===
add_action('wp_ajax_checkevents',        'checkevents');
//add_action('wp_ajax_nopriv_checkevents', 'checkevents');
function checkevents(){
    $res = ['notify'=>0, 'pmsg'=>0, 'gmsg'=>0];
    $_POST['eventID'] = $_POST['eventID'] ?? 0;
    $_POST['eventID'] = (int)$_POST['eventID'];
    //update_user_meta(get_current_user_id(), 'new_pmsg', serialize([])); // reset user

    $res['notify'] = getNotifyUnReadedCount();
    $res['pmsg']   = array_keys( (array)unserialize(get_user_meta(get_current_user_id(), 'new_pmsg', true)) );
    $res['gmsg']   = getCountGMSG($_POST['eventID']);

    echo json_encode($res);
    die();
}


?>