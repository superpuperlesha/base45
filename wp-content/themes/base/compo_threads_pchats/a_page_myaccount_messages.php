<?php
/*
Template Name: My account [Chats private]
*/ ?>

<?php if(!is_user_logged_in()){
	wp_redirect(home_url().'/?upenauth');
	exit();
} ?>

<?php get_header() ?>

<?php page_pchats() ?>

<?php get_footer() ?>