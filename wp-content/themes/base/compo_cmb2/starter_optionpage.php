<?php
/*
Plugin Name: CMB2 Options Page
Plugin URI: https://www.damiencarbery.com/2019/01/use-a-cmb2-options-page-to-populate-ninja-forms-dropdown/
Description: Use CMB2 to add an options page with repeater fields.
Author: Damien Carbery
Version: 0.1
*/




//===small margin admin tables===
add_action('admin_head', 'insert_header_wpse_5w1023');
function insert_header_wpse_5w1023(){
	echo'<style>
			.cmb-type-group .cmb-row, .cmb2-postbox .cmb-row{
				margin: 0px 0px 0px 0px;
				padding: 0px 0px 2px 10px;
			}
		</style>';
}


function getArrPGSoptF(){
    $options[0] = __('Please select an page', 'base');
    $arr = get_posts([
                        'post_type'      => 'page',
                        'posts_per_page' => -1,
                        'orderby'        => 'title',
	                    'order'          => 'ASC',
	                    'post_status'    => 'publish',
                    ]);
    foreach($arr as $post){
        $options[$post->ID] = get_the_title($post->ID);
    }
    return $options;
}
$getArrPGSopt = getArrPGSoptF();
function getArrPGSopt(){
	GLOBAL $getArrPGSopt;
	return $getArrPGSopt;
}


function getArrUsers(){
    $options[0] = __('Please select an user', 'base');
    $users = get_users([
		'blog_id'             => $GLOBALS['blog_id'],
		'role'                => '',
		'role__in'            => [],
		'role__not_in'        => [],
		'meta_key'            => '',
		'meta_value'          => '',
		'meta_compare'        => '',
		'meta_query'          => [],
		'include'             => [],
		'exclude'             => [],
		'orderby'             => 'user_email',
		'order'               => 'ASC',
		'offset'              => '',
		'search'              => '',
		'search_columns'      => [],
		'number'              => '',
		'paged'               => 1,
		'count_total'         => false,
		'fields'              => 'all',
		'who'                 => '',
		'has_published_posts' => null,
		'date_query'          => []
	]);
	foreach($users as $user){
		$options[$user->ID] = $user->user_email;
	}
    return $options;
}



//===Options page===
add_action('cmb2_admin_init', 'dcwd_register_course_dates_options');
function dcwd_register_course_dates_options(){

	//===Appearence submenu===
	$cmb = new_cmb2_box([
		'id'              => 'sopt_pages_block',
		'title'           => __('Site options', 'base'),
		'object_types'    => ['options-page'],
		'option_key'      => 'sopt_pages_block',
        'tab_group'       => 'sopt_pages_block',
		'parent_slug'     => 'themes.php',
		'icon_url'        => 'dashicons-calendar-alt',
		'capability'      => 'publish_posts',
		'position'        => 2,
		'save_button'     => __('Save', 'base'),
		'autoload'        => true,
	]);



	//===GROUP PAGES===
	$group_id = $cmb->add_field([
		'id'          => 'sopt_pages_gp',
		'type'        => 'group',
		'repeatable'  => false,
		'options'     => [
			'group_title' => __('Setup Pages', 'base'),
			'closed'      => true,
		],
	]);
	$cmb->add_group_field($group_id, [
        'id'               => 'sopt_myprofile_edit_pid',
        'name'             => __('My account edit page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_myprofile_view_pid',
        'name'             => __('My account view page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_notification_pid',
        'name'             => __('My account Notifis page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_messages_pid',
        'name'             => __('Chat private page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_gmsgs_pid',
        'name'             => __('Chat group page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_myprofile_delmy_url',
        'name'             => __('My account DELETE page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_events_pid',
        'name'             => __('Events page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_pdf_pid',
        'name'             => __('Generate PDF page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_xls_pid',
        'name'             => __('Generate XLS page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_csv_pid',
        'name'             => __('Generate CSV page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_uploader_pid',
        'name'             => __('Uploader images page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_search_pid',
        'name'             => __('Search people to map page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_recovery_url',
        'name'             => __('Recovery password page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);
    $cmb->add_group_field($group_id, [
        'id'               => 'sopt_logout_pid',
        'name'             => __('Logout page', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrPGSopt',
    ]);




	//===GROUP KEYS===
	$group_id = $cmb->add_field([
		'id'          => 'sopt_keys_gp',
		'type'        => 'group',
		'repeatable'  => false,  // This allows the group fields to be repeated.
		'options'     => [
		'group_title'   => __('Site Keys', 'base'),
		'closed'        => true,  // Repeater fields open by default so that the first repeater field get set.
		],
	]);
	$cmb->add_group_field( $group_id, [
		'name'       => __('Google IPI key', 'base'),
		'id'         => 'sopt_goomap_key',
		'type'       => 'text',
		'attributes' => [
			'required' => 'required',
		],
	]);
	$cmb->add_group_field( $group_id, [
		'name'       => __('STRYPE API key public', 'base'),
		'id'         => 'sopt_strype_key',
		'type'       => 'text',
		'attributes' => [
			'required' => 'required',
		],
	]);
	$cmb->add_group_field( $group_id, [
		'name'       => __('STRYPE API key secret', 'base'),
		'id'         => 'sopt_strype_key_secret',
		'type'       => 'text',
		'attributes' => [
			'required' => 'required',
		],
	]);
	$cmb->add_group_field( $group_id, [
		'name'       => __('PayPal account E-Mail', 'base'),
		'id'         => 'sopt_paypal_email',
		'type'       => 'text',
		'attributes' => [
			'required' => 'required',
		],
	]);
	$cmb->add_group_field($group_id, [
        'id'               => 'sopt_51p_admin_id',
        'name'             => __('System Administrator', 'base'),
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'custom',
        'options_cb'       => 'getArrUsers',
    ]);

}