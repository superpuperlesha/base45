<?php

//===self url===
function getSelfURL(){
    return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}


//===site mail===
function siteMail($to, $message, $file = []){
    $subject = __('Message from site.', 'base');
    $admin   = get_field('sopt_51p_admin_id', 'option');
    $admin   = get_user_by('id', $admin);
    
    if($admin){
        $adminEmail = $admin->user_email;
    }else{
        $adminEmail = get_bloginfo('admin_email');
    }

    $headers = array(
        'from: '.htmlspecialchars(get_bloginfo('name')).' <'.$adminEmail.'>',
        'content-type: text/html',
    );
    return wp_mail($to, $subject, $message, $headers, $file);
}


/*//===generate passwords===
function rand_string($length){
    return substr(str_shuffle(AUTH_KEY), 0, $length);
}*/


function siteDefPaging(\WP_Query $wp_query=null, $echo=true, $params=[]){
    if ( null === $wp_query ) {
        global $wp_query;
    }
    $add_args = [];
    $pages = paginate_links( array_merge( [
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'format'       => '?paged=%#%',
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'total'        => $wp_query->max_num_pages,
            'type'         => 'array',
            'show_all'     => false,
            'end_size'     => 3,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => __( '« Prev' , 'base'),
            'next_text'    => __( 'Next »' , 'base'),
            'add_args'     => $add_args,
            'add_fragment' => ''
        ], $params )
    );

    if ( is_array( $pages ) ) {
        $pagination = '<ul class="pagination">';
        foreach ( $pages as $page ) {
            $pagination .= '<li class="page-item' . (strpos($page, 'current') !== false ? ' active' : '') . '"> ' . str_replace('page-numbers', 'page-link', $page) . '</li>';
        }
        $pagination .= '</ul>';
        if ( $echo ) {
            echo $pagination;
        } else {
            return $pagination;
        }
    }

    return null;
}



//===custom paging===
function sitePaging($baseURL, $numposts, $paged, $postonpage){
    $res = '';
    $posts_per_page = $postonpage;//get_option('posts_per_page');
    $paged = (int)$paged;
    $numposts = (int)$numposts;
    $max_page = ceil($numposts / $postonpage);;
    if ($numposts <= $posts_per_page) {
        return;
    }
    if (empty($paged) || $paged == 0) {
        $paged = 1;
    }
    $pages_to_show = 7;
    $pages_to_show_minus_1 = $pages_to_show - 1;
    $half_page_start = floor($pages_to_show_minus_1 / 2);
    $half_page_end = ceil($pages_to_show_minus_1 / 2);
    $start_page = $paged - $half_page_start;
    if ($start_page <= 0) {
        $start_page = 1;
    }
    $end_page = $paged + $half_page_end;
    if (($end_page - $start_page) != $pages_to_show_minus_1) {
        $end_page = $start_page + $pages_to_show_minus_1;
    }
    if ($end_page > $max_page) {
        $start_page = $max_page - $pages_to_show_minus_1;
        $end_page = $max_page;
    }
    if ($start_page <= 0) {
        $start_page = 1;
    }

    $res .= '<ul class="pagination pagination-sm">';
    if ($paged > 1) {
        $res .= '<li class="page-item prev">
					<a class="page-link" href="' . $baseURL . '&pg=' . ($paged - 1) . '">' . __('Prev', 'base') . '</a>
				 </li>';
    }

    if (1 == 1) {
        // echo '<li class="page-item 222">
        // <a class="page-link btn btn-slick btn-prev" href="'.get_previous_posts_page_link().'">1</a>
        // </li>'; 
    } else {
        $res .= '<li class="page-item disabled">
					<a class="page-link btn btn-slick btn-prev" href="#uID">' . __('Prev', 'base') . '</a>
				 </li>';
    }

    for ($i = $start_page; $i <= $end_page; $i++) {
        if ($i == $paged) {
            $res .= '<li class="page-item active">
						<a class="page-link" href="#uID">' . $i . '</a>
					 </li>';
        } else {
            $res .= '<li class="page-item">
						<a class="page-link" href="' . $baseURL . '&pg=' . $i . '">' . $i . '</a>
					  </li>';
        }
    }

    if (1 == 1) {
        // echo '<li class="page-item">
        // <a class="page-link btn btn-slick btn-next" href="'.get_next_posts_page_link().'"></a>
        // </li>';
    } else {
        $res .= '<li class="page-item disabled">
					<a class="page-link btn btn-slick btn-next" href="#uID">' . __('Next', 'base') . '</a>
				  </li>';
    }

    if ($end_page < $max_page) {
        $res .= '<li class="page-item next">
					<a class="page-link" href="' . $baseURL . '&pg=' . ($paged + 1) . '">' . __('Next', 'base') . '</a>
				 </li>';
    }
    $res .= '</ul>';
    return $res;
}


//===custom paging AJAX===
function sitePagingAJAX($ajaxClass, $numposts, $paged, $postonpage){
    $res = '';
    $posts_per_page = $postonpage;//get_option('posts_per_page');
    $paged = (int)$paged;
    $numposts = (int)$numposts;
    $max_page = ceil($numposts / $postonpage);;
    if ($numposts <= $posts_per_page) {
        return;
    }
    if (empty($paged) || $paged == 0) {
        $paged = 1;
    }
    $pages_to_show = 7;
    $pages_to_show_minus_1 = $pages_to_show - 1;
    $half_page_start = floor($pages_to_show_minus_1 / 2);
    $half_page_end = ceil($pages_to_show_minus_1 / 2);
    $start_page = $paged - $half_page_start;
    if ($start_page <= 0) {
        $start_page = 1;
    }
    $end_page = $paged + $half_page_end;
    if (($end_page - $start_page) != $pages_to_show_minus_1) {
        $end_page = $start_page + $pages_to_show_minus_1;
    }
    if ($end_page > $max_page) {
        $start_page = $max_page - $pages_to_show_minus_1;
        $end_page = $max_page;
    }
    if ($start_page <= 0) {
        $start_page = 1;
    }

    $res .= '<div>';
    if($paged > 1){
        $res .= '<a class="p__prev '.$ajaxClass.'" href="#uID" data-pg="'.($paged - 1).'">-</a>';
    }

    $res .= '<span>
				<span class="pagination__firstNum">'.($paged > 9 ? $paged : '0' . $paged).'</span>
				<span class="pagination__slash"></span>
				<span class="pagination__secondNum">'.($max_page > 9 ? $max_page : '0' . $max_page).'</span>
			</span>';

    if($paged < $max_page){
        $res .= '<a class="p_next '.$ajaxClass.'" href="#uID" data-pg="'.($paged + 1).'">+</a>';
    }
    $res .= '</div>';

    return $res;
}


/*function theme_breadcrumbs(){
      $text['home']     = __('Home', 'webmeridian'); // текст ссылки "Главная"
    $text['category'] = '%s'; // текст для страницы рубрики
    $text['search']   = __('Search results for the query "%s"', 'webmeridian'); // текст для страницы с результатами поиска
    $text['tag']      = __('Posts with tag %s', 'webmeridian'); // текст для страницы тега
    $text['author']   = __('Author posts %s', 'webmeridian'); // текст для страницы автора
    $text['404']      = __('ERROR 404', 'webmeridian'); // текст для страницы 404
    $text['page']     = __('Page %s', 'webmeridian'); // текст 'Страница N'
    $text['cpage']    = __('Page comments %s', 'webmeridian'); // текст 'Страница комментариев N'
    
    $wrap_before    = '<ul class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">'; // открывающий тег обертки
    $wrap_after     = '</ul>'; // закрывающий тег обертки
    $sep            = ''; // разделитель между "крошками"
    $before         = '<li><span class="breadcrumbs__current">'; // тег перед текущей "крошкой"
    $after          = '</span></li>'; // тег после текущей "крошки"
    
    $show_on_home   = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
    $show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
    $show_current   = 1; // 1 - показывать название текущей страницы, 0 - не показывать
    $show_last_sep  = 1; // 1 - показывать последний разделитель, когда название текущей страницы не отображается, 0 - не показывать
  
    
    global $post;
    $home_url       = esc_url(home_url('/'));
    $link           = '<li><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
    $link          .= '<a class="breadcrumbs__link" href="%1$s" itemprop="item"><span itemprop="name">%2$s</span></a>';
    $link          .= '<meta itemprop="position" content="%3$s" />';
    $link          .= '</span></li>';
    $parent_id      = ($post) ?$post->post_parent :'';
    $home_link      = sprintf( $link, $home_url, $text['home'], 1 );
    
    if(get_queried_object_id() == get_option('page_for_posts')){
        echo $wrap_before.$before.$text['home'].$after.$before.__('Blog', 'webmeridian').$after.$wrap_after;
    }
    
    if(is_home() || is_front_page()){
        if ( $show_on_home ) echo $wrap_before . $home_link . $wrap_after;
    }else{
        $position = 0;
        echo $wrap_before;

        if ( $show_home_link ) {
            $position += 1;
            echo $home_link;
        }

        if ( is_category() ) {
            $parents = get_ancestors( get_query_var('cat'), 'category' );
            foreach ( array_reverse( $parents ) as $cat ) {
                $position += 1;
                if ( $position > 1 ) echo $sep;
                echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
            }
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                $cat = get_query_var('cat');
                echo $sep . sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
                echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_current ) {
                    if ( $position >= 1 ) echo $sep;
                    echo $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;
                } elseif ( $show_last_sep ) echo $sep;
            }

        } elseif ( is_search() ) {
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                if ( $show_home_link ) echo $sep;
                echo sprintf( $link, $home_url . '?s=' . get_search_query(), sprintf( $text['search'], get_search_query() ), $position );
                echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_current ) {
                    if ( $position >= 1 ) echo $sep;
                    echo $before . sprintf( $text['search'], get_search_query() ) . $after;
                } elseif ( $show_last_sep ) echo $sep;
            }

        } elseif ( is_year() ) {
            if ( $show_home_link && $show_current ) echo $sep;
            if ( $show_current ) echo $before . get_the_time('Y') . $after;
            elseif ( $show_home_link && $show_last_sep ) echo $sep;

        } elseif ( is_month() ) {
            if ( $show_home_link ) echo $sep;
            $position += 1;
            echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position );
            if ( $show_current ) echo $sep . $before . get_the_time('F') . $after;
            elseif ( $show_last_sep ) echo $sep;

        } elseif ( is_day() ) {
            if ( $show_home_link ) echo $sep;
            $position += 1;
            echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position ) . $sep;
            $position += 1;
            echo sprintf( $link, get_month_link( get_the_time('Y'), get_the_time('m') ), get_the_time('F'), $position );
            if ( $show_current ) echo $sep . $before . get_the_time('d') . $after;
            elseif ( $show_last_sep ) echo $sep;

        } elseif ( is_single() && ! is_attachment() ) {
            if ( get_post_type() != 'post' ) {
                $position += 1;
                $post_type = get_post_type_object( get_post_type() );
                if ( $position > 1 ) echo $sep;
                echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->labels->name, $position );
                if ( $show_current ) echo $sep . $before . get_the_title() . $after;
                elseif ( $show_last_sep ) echo $sep;
            } else {
                $cat = get_the_category(); $catID = $cat[0]->cat_ID;
                $parents = get_ancestors( $catID, 'category' );
                $parents = array_reverse( $parents );
                $parents[] = $catID;
                foreach ( $parents as $cat ) {
                    $position += 1;
                    if ( $position > 1 ) echo $sep;
                    echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
                }
                if ( get_query_var( 'cpage' ) ) {
                    $position += 1;
                    echo $sep . sprintf( $link, get_permalink(), get_the_title(), $position );
                    echo $sep . $before . sprintf( $text['cpage'], get_query_var( 'cpage' ) ) . $after;
                } else {
                    if ( $show_current ) echo $sep . $before . get_the_title() . $after;
                    elseif ( $show_last_sep ) echo $sep;
                }
            }

        } elseif ( is_post_type_archive() ) {
            $post_type = get_post_type_object( get_post_type() );
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                if ( $position > 1 ) echo $sep;
                echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->label, $position );
                echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_home_link && $show_current ) echo $sep;
                if ( $show_current ) echo $before . $post_type->label . $after;
                elseif ( $show_home_link && $show_last_sep ) echo $sep;
            }

        } elseif ( is_attachment() ) {
            $parent = get_post( $parent_id );
            $cat = get_the_category( $parent->ID ); $catID = $cat[0]->cat_ID;
            $parents = get_ancestors( $catID, 'category' );
            $parents = array_reverse( $parents );
            $parents[] = $catID;
            foreach ( $parents as $cat ) {
                $position += 1;
                if ( $position > 1 ) echo $sep;
                echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
            }
            $position += 1;
            echo $sep . sprintf( $link, get_permalink( $parent ), $parent->post_title, $position );
            if ( $show_current ) echo $sep . $before . get_the_title() . $after;
            elseif ( $show_last_sep ) echo $sep;

        } elseif ( is_page() && ! $parent_id ) {
            if ( $show_home_link && $show_current ) echo $sep;
            if ( $show_current ) echo $before . get_the_title() . $after;
            elseif ( $show_home_link && $show_last_sep ) echo $sep;

        } elseif ( is_page() && $parent_id ) {
            $parents = get_post_ancestors( get_the_ID() );
            foreach ( array_reverse( $parents ) as $pageID ) {
                $position += 1;
                if ( $position > 1 ) echo $sep;
                echo sprintf( $link, get_page_link( $pageID ), get_the_title( $pageID ), $position );
            }
            if ( $show_current ) echo $sep . $before . get_the_title() . $after;
            elseif ( $show_last_sep ) echo $sep;

        } elseif ( is_tag() ) {
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                $tagID = get_query_var( 'tag_id' );
                echo $sep . sprintf( $link, get_tag_link( $tagID ), single_tag_title( '', false ), $position );
                echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_home_link && $show_current ) echo $sep;
                if ( $show_current ) echo $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;
                elseif ( $show_home_link && $show_last_sep ) echo $sep;
            }

        } elseif ( is_author() ) {
            $author = get_userdata( get_query_var( 'author' ) );
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                echo $sep . sprintf( $link, get_author_posts_url( $author->ID ), sprintf( $text['author'], $author->display_name ), $position );
                echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_home_link && $show_current ) echo $sep;
                if ( $show_current ) echo $before . sprintf( $text['author'], $author->display_name ) . $after;
                elseif ( $show_home_link && $show_last_sep ) echo $sep;
            }

        } elseif ( is_404() ) {
            if ( $show_home_link && $show_current ) echo $sep;
            if ( $show_current ) echo $before . $text['404'] . $after;
            elseif ( $show_last_sep ) echo $sep;

        } elseif ( has_post_format() && ! is_singular() ) {
            if ( $show_home_link && $show_current ) echo $sep;
            echo get_post_format_string( get_post_format() );
        }

        echo $wrap_after;

    }
}*/
