<?php


//===OPEN SIMPLE PAGE AJAX===
add_action('wp_ajax_openAJAX',        'openAJAX');
add_action('wp_ajax_nopriv_openAJAX', 'openAJAX');
function openAJAX(){
	$_POST['pageid'] = $_POST['pageid'] ?? 0;
	$_POST['pageid'] = (int)$_POST['pageid']; ?>
	
	<section class="container">
		<div class="row">
			<div class="col-md-12"><?php
				echo apply_filters('the_content', get_post_field('post_content', $_POST['pageid']));
	
				/*query_posts(['page_id'=>$_POST['pageid']]);
				if(have_posts()){
					while (have_posts()){
						the_post();
						the_content();
					}
				}*/

				/*$post = get_post($_POST['pageid']);
				echo apply_filters( 'the_content', $post->post_content );*/ ?>
			</div>
		</div>
	</section><?php
	
	die();
}


// //===add post view===
// add_action('wp_ajax_addViewPost',        'addViewPost');
// add_action('wp_ajax_nopriv_addViewPost', 'addViewPost');
// function addViewPost(){
	// update_post_meta((int)$_POST['addViewPost'], 'sp_view', (int)get_post_meta((int)$_POST['addViewPost'], 'sp_view', true)+1);
	// echo (int)get_post_meta((int)$_POST['addViewPost'], 'sp_view', true);
	// die();
// }



// //===LOADMORE blog===
// add_action('wp_ajax_loadpostsmore',        'loadpostsmore');
// add_action('wp_ajax_nopriv_loadpostsmore', 'loadpostsmore');
// function loadpostsmore(){
	// $param = array(
		// 'post_type'       => $_POST['PostTypeLoaded'], 
		// 'posts_per_page'  => $_POST['CountLoadedInc'],
		// 'offset'          => $_POST['CountLoaded'],
	// );
	
	// if($_POST['bsort_order']=='date'){
		
	// }
	// if($_POST['bsort_order']=='view'){
		// $param = array(
			// 'post_type'       => $_POST['PostTypeLoaded'], 
			// 'posts_per_page'  => $_POST['CountLoadedInc'],
			// 'meta_key'		  => 'sp_view',
			// 'orderby'		  => 'meta_value_num',
			// 'order'			  => 'DESC',
			// 'offset'          => $_POST['CountLoaded'],
		// );
	// }
	
	// query_posts($param);
	// while(have_posts()){
		// the_post();
		// get_template_part('template_part/list_item');
	// }
	// die();
// }