<?php
/*
Template Name: Page [insert post && images AJAX]
*/ ?>

<?php if( !is_user_logged_in() ){
	wp_redirect(home_url().'/?upenauth');
	exit();
} ?>

<?php get_header() ?>

<?php page_postimagesajax() ?>
	
<?php get_footer() ?>