<?php


	add_action( 'wp_footer', 'action_function_name_24d72' );
	function action_function_name_24d72(){
		get_template_part('/compo_postimagesajax/starter_add');
	}


	$GLOBALS['arrExt']    = ["image/jpg","image/jpeg","image/png","image/gif","image/svg+xml"];
	$GLOBALS['arrExtImg'] = ["image/jpg","image/jpeg","image/png"];
	$GLOBALS['FSizeM']    = 2000000;


	//===PAGE uploaders===
	function page_postimagesajax(){
		echo'
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>'.__('Title', 'um-theme').'</h3>
					<form action="#uID" method="POST" id="ybop_form" enctype="multipart/form-data">
						<div class="form-group">
							<input type="text" id="ybop_title" name="ybop_title" class="form-control" placeholder="'.__('Title of the project', 'um-theme').'">
						</div>
						
						<div class="form-group">
							<textarea id="ybop_descript" name="ybop_descript" class="form-control" placeholder="'.__('Describe.', 'um-theme').'"></textarea>
						</div>

						<div class="form-group">
							'.wp_nonce_field('ybop_file', 'fileup_nonce').'
							<input type="file" id="ybop_file" name="ybop_file[]" class="form-control" accept="'.implode(', ', $GLOBALS['arrExt']).'" multiple>
							<ul id="ybop_file_table">
								<li id="ybop_file_table_1"></li>
							</ul>
							<div id="ybop_file_errs"></div>
						</div>

						<div class="form-group">
							<input type="hidden" name="action" value="ybop">
							<input type="submit" value="'.__('Publish now!', 'um-theme').'" class="form-control">
						</div>
					</form>
				</div>
			</div>
		</div>';
	}


	//===DELETE group mssage===
	add_action('wp_ajax_getuploaderajax',        'getuploaderajax');
	//add_action('wp_ajax_nopriv_getuploaderajax', 'getuploaderajax');
	function getuploaderajax(){
	    page_postimagesajax();
	    die();
	}


	//=== add new order ===
	add_action('wp_ajax_ybop', function(){
	    $res = ['err'=>'', 'txt'=>''];
	    $_POST['ybop_title']    = $_POST['ybop_title']    ?? '';
	    $_POST['ybop_descript'] = $_POST['ybop_descript'] ?? '';
	    
	    $_POST['ybop_title']    = sanitize_text_field($_POST['ybop_title']);
	    $_POST['ybop_descript'] = sanitize_text_field($_POST['ybop_descript']);

	    if( strlen($_POST['ybop_title'])<7 ){
	        $res['err'] =__('Enter title no less 7.', 'um-theme');
	    }
	    if( strlen($_POST['ybop_descript'])<16 ){
	        $res['err'] =__('Enter description no less 16.', 'um-theme');
	    }
	    
	    if( !$res['err'] ){

	        $post_id = wp_insert_post([
	            'post_title'   => $_POST['ybop_title'],
	            'post_content' => $_POST['ybop_descript'],
	            'post_type'    => 'post',
	            'post_status'  => 'publish',
	            'post_author'  => get_current_user_id(),
	        ]);

	        if( is_int($post_id) && $post_id>0 ){
	            //wp_set_post_terms($post_id, $_POST['ybop_category'], 'termname');
	            //update_post_meta($post_id, 'xxx', '');
	            $arr = [];

	            //$res['err'] .= print_r( $_FILES['ybop_file'], true );
	            //===upload attachment===
	            if( isset($_FILES['ybop_file']['tmp_name']) && is_array($_FILES['ybop_file']['tmp_name']) && isset($_POST['fileup_nonce']) && wp_verify_nonce($_POST['fileup_nonce'], 'ybop_file') ){
	                for($i=0;$i<count($_FILES['ybop_file']['tmp_name']);$i++){
	                    if( isset($_FILES['ybop_file']['tmp_name'][$i]) && $_FILES['ybop_file']['tmp_name'][$i] ){
	                        if( (int)$_FILES['ybop_file']['error'][$i]==0 ){
	                            require_once( ABSPATH . 'wp-admin/includes/admin.php' );
	                            $ff['name']     = $_FILES['ybop_file']['name'][$i];
	                            $ff['type']     = $_FILES['ybop_file']['type'][$i];
	                            $ff['tmp_name'] = $_FILES['ybop_file']['tmp_name'][$i];
	                            $ff['error']    = $_FILES['ybop_file']['error'][$i];
	                            $ff['size']     = $_FILES['ybop_file']['size'][$i];
	                            $file_return = wp_handle_upload( $ff, array('test_form'=>false) );
	                            if( isset( $file_return['error'] ) || isset( $file_return['upload_error_handler'] ) ) {
	                                $res['err'] .= __('Error uploading file!', 'base');
	                            }else{
	                                $filename = $file_return['file'];
	                                $attachment = array(
	                                    'post_mime_type' => $file_return['type'],
	                                    'post_title'     => htmlspecialchars(basename($filename)),
	                                    'post_content'   => '',
	                                    'post_status'    => 'inherit',
	                                    'guid'           => $file_return['url'],
	                                );
	                                $attachment_id = wp_insert_attachment( $attachment, $file_return['url'] );
	                                //update_post_meta($post_id, 'job_files', $file_return['url']);
	                                $arr[] = $file_return['url'];
	                            }
	                        }else{
	                            $res['err'] .= __('Error inserting file:', 'um-theme').$_FILES['ybop_file']['name'][$i].'. Error:'.(int)$_FILES['ybop_file']['error'][$i].'<br/>';
	                        }
	                    }
	                }
	                $res['err'] .= __('DONE.', 'um-theme');
	            }
	            update_post_meta($post_id, 'job_files', serialize($arr));
	        }else{
	            $res['err'] .= __('Error inserting job.', 'um-theme');
	        }
	    }

	    echo json_encode($res);
	    die();
	});

?>