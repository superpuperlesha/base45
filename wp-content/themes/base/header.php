<?php
/**
* The header.
*
* @package base
*/
?>
<!DOCTYPE html>
<html <?php language_attributes() ?>>
	<head>
  		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta charset="<?php bloginfo('charset') ?>">
		<?php wp_head() ?>
		<style>a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:'';content:none}table{border-collapse:collapse;border-spacing:0}</style>
	</head>
	<body <?php body_class() ?>>
		
		<?php //wp_body_open() ?>

		<header class="container">
			<div class="row">
				<div class="col-md-12">
					<nav class="navbar navbar-expand-lg navbar-light bg-light">
						<a href="<?= esc_url(home_url()) ?>" class="navbar-brand">
							<!--<img src="<?= esc_url(get_template_directory_uri()) ?>/img/logo.png" alt="<?= htmlspecialchars(get_bloginfo('name')) ?>">-->
							<svg width="50" height="50"><circle cx="25" cy="25" r="25"/></svg>
						</a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarScroll"><?php
							wp_nav_menu([
								'theme_location'  => 'HeadMenu',
								'menu'            => '', 
								'container'       => '', 
								'container_class' => '', 
								'container_id'    => '',
								'menu_class'      => 'navbar-nav mr-auto my-2 my-lg-0 navbar-nav-scroll', 
								'menu_id'         => 'MMid1',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 0,
								'walker'          => new BT5_Menu(),
							]) ?>
							
							<?= get_search_form() ?>
							
							<div>
								<?php if(is_user_logged_in()){ ?>
									<img id="mainavaacc" class="header__profile-img" src="<?= get_myavatar_url(get_current_user_id(), 'thumbnail_64x64') ?>" width="48" height="48" alt="avatar"><?php
						            if( is_user_logged_in() && (int)get_user_meta(get_current_user_id(), 'admin_payment', true)==0 && !isset($_GET['AdminHello']) ){
						                $current_user = wp_get_current_user();
						                $tsdt         = (int)get_field('sopt_ntf_int_block_min', 'option')*60;
						                $tsreg        = (int)strtotime($current_user->user_registered);
						                $sec          = $tsdt - (time()-$tsreg);
						                if($sec > 0){
						                    echo __('Free time is: ', 'base').$sec.__(' sec.', 'base').'<a href="#uID" class="link" id="gotopayment">'.__('to complete payment', 'base').'</a>';
						                }
						            } ?>
						            <ul>
										<?php $id = get_option('woocommerce_myaccount_page_id') ?>
						            	<li class="w51p_menu_csv">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>">WOO: <?= get_the_title($id) ?></a>
						            	</li>
						            	<?php $id = get_field('sopt_myprofile_edit_pid', 'option') ?>
						            	<li class="menu_myacc_edit">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
						            	</li>
						            	<?php $id = get_field('sopt_logout_pid', 'option') ?>
						            	<li class="menu_logout">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
						            	</li>
						            	<?php $id = get_field('sopt_notification_pid', 'option') ?>
						            	<li class="menu_notices <?= (getNotifyUnReadedCount(get_current_user_id())>0 ?'active' :'') ?>">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>/?unread" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
						            	</li>
						            	<?php $id = get_field('sopt_messages_pid', 'option') ?>
						            	<li class="menu_pchats <?= (getPMsgUnReadedCount(get_current_user_id())>0    ?'active' :'') ?>">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
						            	</li>
						            	<?php $id = get_field('sopt_search_pid', 'option') ?>
						            	<li class="w51p_menu_search">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
						            	</li>
						            	<?php $id = get_field('sopt_events_pid', 'option') ?>
						            	<li class="w51p_menu_events">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
						            	</li>
						            	<?php $id = get_field('sopt_gmsgs_pid', 'option') ?>
						            	<li class="w51p_menu_gmsgs">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
						            	</li>
										<?php $id = get_field('sopt_uploader_pid', 'option') ?>
						            	<li class="w51p_menu_uploader">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
						            	</li>
						            	<?php $id = get_field('sopt_pdf_pid', 'option') ?>
						            	<li class="w51p_menu_pdf">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>" target="_blank"><?= get_the_title($id) ?></a>
						            	</li>
						            	<?php $id = get_field('sopt_xls_pid', 'option') ?>
						            	<li class="w51p_menu_xls">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>" target="_blank"><?= get_the_title($id) ?></a>
						            	</li>
						            	<?php $id = get_field('sopt_csv_pid', 'option') ?>
						            	<li class="w51p_menu_csv">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>" target="_blank"><?= get_the_title($id) ?></a>
						            	</li>
						            	<?php $id = get_field('sopt_twig_pid', 'option') ?>
						            	<li class="w51p_menu_csv">
						            		<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>" target="_blank"><?= get_the_title($id) ?></a>
						            	</li>
						            </ul><?php
								}else{ ?>
									<ul>
						            	<li class="ds_float_login"><a href="#uID"><?= __('Login', 'base') ?></a></li>
						            </ul><?php
								} ?>
							</div>

						</div>
					</nav>
				</div>
			</div>
		</header>
		
		<main id="base_main">