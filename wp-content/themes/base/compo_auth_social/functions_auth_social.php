<?php


//===AUTH GOOGLE all===
define('auth_google_app_secret',         '9fWpnD5kij5-l91scIInAQzu'); // APP secret MY
define('auth_google_app_id',             '72091751399-bpdloof1avq2mtuce901rjfj26jjiqer.apps.googleusercontent.com');
define('auth_google_app_redirect_auth',  esc_url(home_url()).'/?auth_google=1'); // app redirect url after auth any roles in google
define('auth_google_url',                'https://accounts.google.com/o/oauth2/auth?'.urldecode(http_build_query(array('redirect_uri'=>auth_google_app_redirect_auth, 'response_type'=>'code', 'client_id'=>auth_google_app_id, 'scope'=>'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'))));
function getGOOGLEUserInfo(){
		$params = array(
			'client_id'     => auth_google_app_id,
			'client_secret' => auth_google_app_secret,
			'redirect_uri'  => auth_google_app_redirect_auth,
			'grant_type'    => 'authorization_code',
			'access_type'   => 'offline',
			'code'          => $_GET['code']
		);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://accounts.google.com/o/oauth2/token');
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($curl);
		curl_close($curl);
		$tokenInfo = json_decode($result, true);

		if(isset($tokenInfo['access_token'])){
			$params['access_token'] = $tokenInfo['access_token'];
			$userInfo = json_decode(@file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo?'.urldecode(http_build_query($params))), true);
			if(isset($userInfo['id'])){
				setSNREG($userInfo);
			}
		}else{
			$GLOBALS['SNR_openreg'] = ['error'=>$tokenInfo['error']];
		}
}
if( isset($_GET['auth_google']) && isset($_GET['code']) ){
	getGOOGLEUserInfo();
}




//===AUTH FACEBOOK all===
define('auth_fb_app_secret',                         'ac6f220e8507c8d08f9536acbf497386'); // APP secret
define('auth_fb_app_id',                             '748497045806428'); // APP ID
define('auth_fb_app_redirect_auth',                  esc_url(home_url()).'/?auth_fb=1'); // app redirect url after auth any roles in FB
define('auth_fb_url',                                'https://www.facebook.com/dialog/oauth?'.urldecode(http_build_query(array('client_id'=>auth_fb_app_id, 'redirect_uri'=>auth_fb_app_redirect_auth, 'response_type'=>'code', 'scope'=>'email'))));
function getFBUserInfo(){
	$params = array(
		'client_id'       => auth_fb_app_id,
		'client_secret'   => auth_fb_app_secret,
		'redirect_uri'    => auth_fb_app_redirect_auth,
		'code'            => $_GET['code'],
	);
	$tokenInfo = json_decode(@file_get_contents('https://graph.facebook.com/oauth/access_token?'.http_build_query($params)));
	if(isset($tokenInfo->access_token)){
		$params = array('access_token' => $tokenInfo->access_token);
		$userInfo = json_decode(file_get_contents('https://graph.facebook.com/me?fields=id,first_name,last_name,email&appsecret_proof='.hash_hmac('sha256', $tokenInfo->access_token, auth_fb_app_secret).'&'.urldecode(http_build_query($params))), true);
		if(isset($userInfo['id'])){
			setSNREG($userInfo);
		}
	}else{
		$GLOBALS['SNR_openreg'] = ['error'=>$tokenInfo['error']];
	}
}
if( isset($_GET['auth_fb']) && isset($_GET['code']) ){
	getFBUserInfo();
}




//===AUTH TWITTER===
define('auth_tv_app_secret',                         'ac6f220e8507c8d08f9536acbf497386'); // APP secret
define('auth_tv_app_id',                             '748497045806428'); // APP ID
define('auth_tv_app_redirect_auth',                  esc_url(home_url()).'/?auth_tw=1'); // app redirect url after auth any roles in TW
define('auth_tv_url',                                'https://api.twitter.com/oauth2/token?'.urldecode(http_build_query(array('client_id'=>auth_tv_app_id, 'redirect_uri'=>auth_tv_app_redirect_auth, 'response_type'=>'code', 'scope'=>'email'))));






function setSNREG($arr){
	$user = get_user_by('email', $arr['email']);
	if($user){
		clean_user_cache($user->ID);
		wp_clear_auth_cookie();
		wp_set_current_user( $user->ID );
		wp_set_auth_cookie( $user->ID , true, false);
		update_user_caches($user);
		if(is_user_logged_in()){
			update_user_meta($user->ID, 'login_count', ((int)get_user_meta($user->ID, 'login_count', true)+1) );
			wp_redirect( home_url() );
			exit;
		}
	}else{
		$first_name = $arr['first_name'] ?? $arr['given_name']; //google
		$last_name  = $arr['last_name']  ?? $arr['family_name']; //fb
		$GLOBALS['SNR_openreg'] = ['email'=>$arr['email'], 'first_name'=>$first_name, 'last_name'=>$last_name];
		wp_redirect( home_url().'?SNR_openreg_email='.$arr['email'].'&SNR_openreg_first_name='.$first_name.'&SNR_openreg_last_name='.$last_name );
		exit;
	}
}






//===AUTH linkedin all===
/*define('auth_li_app_secret',                         'PElGuAdzgTy99HAC'); // APP secret
define('auth_li_app_id',                             '866gtwf9we8yfl'); // APP ID
define('auth_li_app_redirect',                       getSEFfile('login-page.php').'?auth_li=1'); // app redirect url after auth any roles in LI
define('auth_li_app_redirect_talent_getinfo',        getSEFfile('signup-page.php').'?reg_talent_li=1'); // app redirect url after getting info in li
define('auth_li_app_redirect_employer_getinfo',      getSEFfile('signup-page.php').'?reg_employer_li=1'); // app redirect url after getting info in li
define('auth_li_app_redirect_recruiter_getinfo',     getSEFfile('signup-page.php').'?reg_recruiter_li=1'); // app redirect url after getting info in li
define('auth_li_url',                                'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id='.auth_li_app_id.'&redirect_uri='.urlencode(auth_li_app_redirect                  ).'&state=fooobar&scope=r_liteprofile+r_emailaddress+w_member_social');
define('auth_li_talent_url',                         'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id='.auth_li_app_id.'&redirect_uri='.urlencode(auth_li_app_redirect_talent_getinfo   ).'&state=fooobar&scope=r_liteprofile+r_emailaddress+w_member_social');
define('auth_li_employer_url',                       'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id='.auth_li_app_id.'&redirect_uri='.urlencode(auth_li_app_redirect_employer_getinfo ).'&state=fooobar&scope=r_liteprofile+r_emailaddress+w_member_social');
define('auth_li_recruiter_url',                      'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id='.auth_li_app_id.'&redirect_uri='.urlencode(auth_li_app_redirect_recruiter_getinfo).'&state=fooobar&scope=r_liteprofile+r_emailaddress+w_member_social');
if(isset($_GET['reg_talent_li'])){
	define('auth_li_redirect', auth_li_app_redirect_talent_getinfo);
}elseif(isset($_GET['reg_employer_li'])){
	define('auth_li_redirect', auth_li_app_redirect_employer_getinfo);
}elseif(isset($_GET['reg_recruiter_li'])){
	define('auth_li_redirect', auth_li_app_redirect_recruiter_getinfo);
}else{
	define('auth_li_redirect', auth_li_app_redirect);
}*/





