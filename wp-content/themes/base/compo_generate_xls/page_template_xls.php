<?php
/*
Template Name: Page [XLS]
*/

	//page_xls();
	
	require dirname(__FILE__).'/xls/vendor/autoload.php';
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

    $nd_dog_status = $_GET['nd_dog_status'] ?? 0;
    
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    
    
    $query1 = new WP_Query(array(
            'posts_per_page' => 999999,
            'offset'         => 0,
            'post_type'      => 'sdods',
            'post_status'    => 'publish',
            'meta_query'     => array(
                array(
                    'key'    => 'nd_dog_status',
                    'value'  => $nd_dog_status,
                ),
            ),
    ));
    $i = 1;
    $sheet->setCellValue('A'.$i, 'Field 1');
    $sheet->setCellValue('B'.$i, 'Field 2');
    $sheet->setCellValue('C'.$i, 'Field 3');
    while($query1->have_posts()){
        $query1->the_post();
        $i++;
        $sheet->setCellValue('A'.$i, get_the_date('d.m.Y'));
        $sheet->setCellValue('B'.$i, get_post_meta(get_the_id(), 'nd_program', true));
        $sheet->setCellValue('C'.$i, get_post_meta(get_the_id(), 'nd_dog_sds', true));
    }
    
    $file = dirname(__FILE__).'/tmp/'.microtime(true).'.xlsx';
    $writer = new Xlsx($spreadsheet);
    $writer->save($file);
    //$writer->save('php://output');
    
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);

?>