<div class="mt-1 text-center" data-toggle="tooltip" data-placement="top" title="<?= __('View user account', 'base') ?>">
	<a href="#uID" class="dc_myacc_view" data-userid="<?= $args['ID'] ?>">
		<img src="<?= get_myavatar_url($args['ID'], 'thumbnail_100x100') ?>" alt="Ava" width="100" height="100" class="img-fluid border border-success rounded">
	</a>
</div>
<div class="mt-1 text-center"><?php
	if((int)get_user_meta($args['ID'], 'usr_imeet', true)){ ?>
		<h5 class="d-inline"><i class="fa fa-address-book" data-toggle="tooltip" data-placement="top" data-toggle="tooltip" data-placement="top" title="<?php _e('I met her in person', 'base') ?>"></i></h5><?php
	}

	if((int)get_user_meta($args['ID'], 'usr_isee', true)){ ?>
		<h5 class="d-inline"><i class="fa fa-user-plus"    data-toggle="tooltip" data-placement="top" data-toggle="tooltip" data-placement="top" title="<?php _e('I verify that her profile image is a true likeness', 'base') ?>"></i></h5><?php
	}

	if( get_current_user_id() !== (int)$args['ID'] ){ ?>
		<h5 class="d-inline"><i id="usr_fav_<?= $args['ID'] ?>" class="fa <?= (get_yn_user_fav($args['ID']) ?'fa-star' :'fa-star-o') ?> dc_favicon_addel" data-favid="<?= $args['ID'] ?>" data-toggle="tooltip" data-placement="top" title="<?= __('Favarit', 'base') ?>"></i></h5><?php
	} ?>
	
</div>