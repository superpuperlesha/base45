<script>

    var hartFilled       = '<?= hartFilled ?>';
    var hartNFilled      = '<?= hartNFilled ?>';

	document.addEventListener('DOMContentLoaded', function(){
		
		//===MENU SEARCH===
		function w51p_menu_search(pg='', event_search_radius='', event_search_txt='', event_search_order='', pURL){
			setURLpage(pURL);
			$('#base_main').fadeTo(fadeSpeed, 0, 'linear', function(){  });
			$.ajax({
				type:   'POST',
				url:    WPajaxURL,
				data:{
						action:              'menusearch',
						pg:                  pg,
						event_search_radius: event_search_radius,
						event_search_txt:    event_search_txt,
						event_search_order:  event_search_order
					 },
				success: function(data, textStatus, XMLHttpRequest) {
					$('#base_main').html(data);
					$('#base_main').fadeTo(fadeSpeed, 1, 'linear', function(){  });
					init123();
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});
		}

		//===MENU page SEARCH===
		$(document).on('click', '.w51p_menu_search', function(event){
			event.preventDefault();
			var pURL = $(this).find('a').attr('href');
			w51p_menu_search('', '', '', '', pURL);
		});

		//===FILTER page SEARCH===
		$(document).on('click', '.event_search_submitter', function(event){
			event.stopPropagation();
			event.preventDefault();
			var pg                  = $('#event_search_pg').val();
			var event_search_radius = $('#event_search_radius').val();
			var event_search_txt    = $('#event_search_txt').val();
			var event_search_order  = $('#event_search_order').val();
			w51p_menu_search(pg, event_search_radius, event_search_txt, event_search_order);
		});

		//===PAGING page SEARCH===
		$(document).on('click', '.paging_search', function(){
			var pg                  = $(this).attr('data-pg');
			var event_search_radius = $('#event_search_radius').val();
			var event_search_txt    = $('#event_search_txt').val();
			var event_search_order  = $('#event_search_order').val();
			w51p_menu_search(pg, event_search_radius, event_search_txt, event_search_order);
		});

		//===SEARCH pam init===
		function init123(){
			if( typeof dc_search_map_arr !== 'undefined' ){
				//===MAP INIT===
				const myLatLng = { lat: dc_search_map_my_lt, lng: dc_search_map_my_ln };
				const map = new google.maps.Map(document.getElementById('dc_search_map_box'), {
					zoom: 14,
					center: myLatLng,
					styles: csmapstyle
				});
				new google.maps.Marker({position:myLatLng, map, title:'My location'});

				//===circle===
				const cityCircle = new google.maps.Circle({
						strokeColor: "#FF0000",
						strokeOpacity: 0.8,
						strokeWeight: 1,
						fillColor: "#FF0000",
						fillOpacity: 0.15,
						map,
						center: myLatLng,
						radius: dc_search_map_radius,
					});

				var bounds = new google.maps.LatLngBounds();
				bounds.extend(myLatLng);

				//===markers===
				dc_search_map_arr.forEach(function(element) {
					var myLatLng1 = { lat: element.lt, lng: element.ln };
					new google.maps.Marker({position:myLatLng1, map, title:element.title});
					bounds.extend(myLatLng1);
				});
				map.fitBounds(bounds);
			}
		}
		init123();
		
		//===DEL FROM FAVORITES===
		$(document).on('click', '.dc_favicon_del', function(){
			var favid = $(this).attr('data-favid');
			var eid = $(this).attr('id');
			$('#dc_favlist_box').html(Loading);

			if(favid>0){
				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data: {
							action: 'favdel',
							favid:  favid,
						  },
					success:     function(data, textStatus, XMLHttpRequest) {
						$('#dc_favlist_box').html(data);
						$('#usr_fav_i_'+favid).attr('src', hartNFilled);
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
			}
		});
		
		
		//===ADD/DEL to FAVORITES===
		$(document).on('click', '.dc_favicon_addel', function(){
			var favid = $(this).attr('data-favid');
			var eid = $(this).attr('id');
			$('#dc_favlist_box').html(Loading);

			if(favid>0){
				$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data: {
							action: 'favadddel',
							favid:  favid,
						  },
					success:     function(data, textStatus, XMLHttpRequest) {
						data = JSON.parse(data);
						if(data.itype=='1'){
							$('#'+eid).attr('src', hartFilled);
						}else{
							$('#'+eid).attr('src', hartNFilled);
						}
						$('#dc_favlist_box').html(data.txt);
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
			}
		});
		
	});
</script>