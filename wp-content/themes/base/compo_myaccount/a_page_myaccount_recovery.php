<?php
/*
Template Name: My account [recovery password]
*/
get_header() ?>

<article class="container">
	<div class="row">
		<div class="col-md-12">
			
			<?php while(have_posts()){
				the_post();
				echo'<h1>'.get_the_title().'</h1>';
				the_content();
			} ?>

			<form action="<?= esc_url(home_url()) ?>" method="POST">
		  		<div class="form-group">
		        	<input type="password" class="form-control" name="usr_prestset_pass" placeholder="<?= __('New password', 'base') ?>">
		        </div>
		        <div class="form-group">
		        	<input type="hidden" name="usr_prestset_secret" value="<?= $_GET['usersecretrestore'] ?? '0' ?>">
				</div>
				<input type="submit" class="btn btn-primary"        value="<?= __('Change password', 'base') ?>">
			</form>
		</div>
	</div>
</article>
	
<?php get_footer() ?>