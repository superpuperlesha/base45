<?php
/*
Template Name: My account [Delete my Account]
*/ ?>

<?php if(!is_user_logged_in()){
	wp_redirect(home_url().'/?upenauth');
	exit();
} ?>

<?php if(isset($_POST['dc_delete_myacc'])){
	$user = get_user_by('email', $_POST['dc_delete_myacc']);
	if( $user && $user->user_email == $_POST['dc_delete_myacc'] ){
		if(in_array(s51m_user_role, $user->roles)){
			require_once(ABSPATH.'wp-admin/includes/user.php');
			wp_delete_user(get_current_user_id());
			wp_redirect( home_url() ); 
			exit;
		}else{ 
			$err = __('A user with this role cannot be deleted!', 'base');
		}
	}else{
		$err = __('The email address is not correct!', 'base');
	}
} ?>

<?php get_header() ?>

    <section class="container">
    	<div class="row">
        	<div class="col-md-12"><?php
				while(have_posts()){
					the_post();
					echo'<h1>'.get_the_title().'</h1>';
					the_content();
				} ?>
	            <p class="text-danger"><?= (isset($err) ?$err :'') ?></p>
	        </div>
		</div>
        <div class="container">
        	<div class="row">
        		<div class="col-md-12">
		            <form action="<?= get_page_link(get_queried_object_id()) ?>" method="POST">
		                <h3><?= __('Write your email address and click delete!', 'base') ?></h3>
		                <div class="form-group">
							<label for="exampleFormControlInput1"><?= __('Email', 'base') ?></label>
							<input type="email" class="form-control" id="exampleFormControlInput1" placeholder="<?= __('Email', 'base') ?>">
						</div>
		                <button type="submit" class="btn btn-primary"><?= __('Delete my account', 'base') ?></button> 
		            </form>
		        </div>
	        </div>
        </div>
    </section>
	
<?php get_footer() ?>