<?php


define('s51m_user_role', 'sprole');
add_role(s51m_user_role, 'Front user role');
define('verified_y',     esc_url(get_template_directory_uri()).'/img/icon-verified.svg');
define('verified_n',     esc_url(get_template_directory_uri()).'/img/icon-verified-no.svg');
define('avaMaxSize',     2000000);


// PayPal functions
include( get_template_directory() . '/compo_myaccount/functions_api_payment.php' );


add_action( 'wp_footer', 'action_function_name_2473' );
function action_function_name_2473(){
    get_template_part('/compo_myaccount/starter_add');
}


//===MY ACCOUNT EDIT===
function page_myaccedit($uID = 0){
    //===CHECK PAYMENT===
    $checkUserCS = checkUserCS(true);
    if($checkUserCS){
        echo $checkUserCS;
        exit();
    }

    $uID = (int)$uID;

    //===only admin can edit other account===
    if( !current_user_can('administrator') || !$uID ){
        $uID = get_current_user_id();
    }
    $userdata = get_user_by('id', $uID); ?>

    <section class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?= get_the_title(get_field('sopt_myprofile_edit_pid', 'option')) ?></h1>

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="myac_1_tab" data-toggle="tab" href="#myac_1" role="tab" aria-controls="myac_1" aria-selected="true" ><?= __('Account details', 'base') ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"        id="myac_2_tab" data-toggle="tab" href="#myac_2" role="tab" aria-controls="myac_2" aria-selected="false"><?= __('Change password', 'base') ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"        id="myac_3_tab" data-toggle="tab" href="#myac_3" role="tab" aria-controls="myac_3" aria-selected="false"><?= __('Notifications settings', 'base') ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"        id="myac_4_tab" data-toggle="tab" href="#myac_4" role="tab" aria-controls="myac_4" aria-selected="false"><?= __('Account deletion', 'base') ?></a>
                    </li>
                </ul>

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="myac_1" role="tabpanel" aria-labelledby="myac_1_tab">
                        <h2><?= __('Edit My Profile (public)', 'base') ?></h2>

                        <div id="usr_myacupdateava_err" class="text-danger"></div>

                        <form action="#uID" method="POST" id="usr_myacupdateava_form" enctype="multipart/form-data">
                            <?php wp_nonce_field('usr_ava', 'fileup_nonce') ?>
                            <img id="mainavampf" src="<?= get_myavatar_url($uID, 'thumbnail_64x64') ?>" alt="Ava" width="64" height="64">
                            <input type="file" id="usr_ava" name="usr_ava" data-error="<?= __('Only (JPG, JPEG, PNG) files!', 'base') ?>" data-filesizeerr="<?= __('File size no more 2mB.', 'base') ?>">
                            <input type="hidden" id="usr_id" name="usr_id" value="<?= $uID ?>">
                            <input type="hidden" name="action" value="updatemyaccava">
                        </form>

                        <form action="#uID" method="POST" id="usr_myacupdate_form">
                            <a href="<?= get_page_uri(get_field('sopt_myprofile_view_pid', 'option')) ?>/?profile=<?= $uID ?>" data-userid="<?= $uID ?>" class="link dc_myacc_view">
                                <?= __('View My Profile', 'base') ?>
                            </a>
                            <div class="form-group">
                                <input type="text"   name="usr_fname" id="usr_fname" value="<?= $userdata->user_firstname ?>"  placeholder="<?= __('First Name', 'base') ?>">
                            </div>
                            <div class="form-group">
                                <input type="text"   name="usr_lname" id="usr_lname" value="<?= $userdata->user_lastname ?>"   placeholder="<?= __('Last Name', 'base') ?>">
                            </div>
                            <div class="form-group">
                                <input type="email"  name="usr_email" id="usr_email" value="<?= $userdata->user_email ?>"      placeholder="<?= __('E-Mail', 'base') ?>" readonly>
                            </div>
                            <div class="form-group">
                                <input type="text"   name="usr_zip"     id="usr_zip"     value="<?= get_user_meta($uID, 'usr_zip_loc', true) ?>" placeholder="<?= __('Address', 'base') ?>">
                                <input type="hidden" name="usr_zip_loc" id="usr_zip_loc" value="<?= get_user_meta($uID, 'usr_zip_loc', true) ?>">
                                <input type="hidden" name="usr_zip_lt"  id="usr_zip_lt"  value="<?= get_user_meta($uID, 'usr_zip_lt',  true) ?>">
                                <input type="hidden" name="usr_zip_ln"  id="usr_zip_ln"  value="<?= get_user_meta($uID, 'usr_zip_ln',  true) ?>">
                            </div>
                            <div class="form-group">
                                <label class="form__label form-login__label" for="usr_visible">
                                    <input type="checkbox" name="usr_visible" id="usr_visible" <?= ((int)get_user_meta($uID, 'usr_visible', true) ? 'checked="checked"' : '') ?> >
                                    <?= __('My profile is visible to other members', 'base') ?>
                                </label>
                            </div>
                            
                            <div id="usr_myacupdate_mssgbox" class="text-danger"></div>
                            <input type="hidden" name="usr_id" value="<?= $uID ?>">
                            <input type="hidden" name="action" value="updatemyaccusr">
                            <a href="#uID" id="usr_myacupdate_start" class="btn btn-primary"><?= __('Save My Profile', 'base') ?></a>
                            <a href="<?= get_page_uri(get_field('sopt_myprofile_view_pid', 'option')) ?>/?profile=<?= $uID ?>" class="btn btn-primary" data-userid="<?= $uID ?>"><?= __('View My Profile', 'base') ?></a>
                            <button type="button" id="chyear_action" class="btn btn-primary"><?= __('Question to admin', 'base') ?></button>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="myac_2" role="tabpanel" aria-labelledby="myac_2_tab">
                        <h2><?= __('Change my Password', 'base') ?></h2>
                        <form action="#" method="POST" class="form form-acc-password">
                            <div class="form-group">
                                <input type="password" id="usr_old_pass" name="usr_old_pass">
                                <label for="usr_old_pass"><?= __('Old password', 'base') ?></label>
                            </div>
                            <div class="form-group">
                                <input type="password" id="usr_ch_pass" name="usr_ch_pass">
                                <label for="usr_ch_pass"><?= __('New password', 'base') ?></label>
                            </div>
                            <div class="form-group">
                                <input type="password" id="usr_ch_passc" name="usr_ch_passc">
                                <label for="usr_ch_passc"><?= __('Confirm New password', 'base') ?></label>
                            </div>
                            <div id="usr_ch_pass_msgbox" class="text-danger"></div>
                            <input type="hidden" id="usr_ch_pass_uid" name="usr_ch_pass_uid" value="<?= $uID ?>">
                            <a href="#uID" id="usr_ch_pass_start" class="btn btn-primary"><?= __('Change password', 'base') ?></a>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="myac_3" role="tabpanel" aria-labelledby="myac_3_tab">
                        <h3><?= __('Notify me by email when:', 'base') ?></h3>
                        <form action="#" class="form form-acc-notifications" method="POST">
                            <div class="form-group">
                                <label class="form__label form-login__label" for="usr_cf_1">
                                    <input type="checkbox" id="usr_cf_1" name="usr_cf_1" class="form-check-input" <?= (get_user_meta(get_current_user_id(), 'usr_cf_1', true) ? 'checked="checked"' : '') ?>>
                                    <?= __('Email then "new private message"', 'base') ?>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="form__label form-login__label" for="usr_cf_2">
                                    <input type="checkbox" id="usr_cf_2" name="usr_cf_2" class="form-check-input" <?= (get_user_meta(get_current_user_id(), 'usr_cf_2', true) ? 'checked="checked"' : '') ?>>
                                    <?= __('Email then "new group message"', 'base') ?>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="form__label form-login__label" for="usr_cf_3">
                                    <input type="checkbox" id="usr_cf_3" name="usr_cf_3" class="form-check-input" <?= (get_user_meta(get_current_user_id(), 'usr_cf_3', true) ? 'checked="checked"' : '') ?>>
                                    <?= __('Email then "an invite"', 'base') ?>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="form__label form-login__label" for="usr_cf_4">
                                    <input type="checkbox" id="usr_cf_4" name="usr_cf_4" class="form-check-input" <?= (get_user_meta(get_current_user_id(), 'usr_cf_4', true) ? 'checked="checked"' : '') ?>>
                                    <?= __('Email then "update invite"', 'base') ?>
                                </label>
                            </div>
                            <div id="usr_ch_confsets_msgbox" class="text-danger"></div>
                            <input type="hidden" id="usr_cf_uid" name="usr_cf_uid" value="<?= $uID ?>">
                            <a href="#uID" id="usr_cf_start" class="btn btn-primary"><?= __('Save', 'base') ?></a>
                        </form>
                    </div>

                    <div class="tab-pane account-deactivation fade" id="myac_4" role="tabpanel" aria-labelledby="myac_4_tab">
                        <h2><?= __('Account deletion', 'base') ?></h2><?php
                        if (!current_user_can('administrator')) { ?>
                            <a href="<?= get_field('sopt_myprofile_delmy_url', 'option') ?>" class="btn btn--accent"><?= __('Delete My Profile', 'base') ?></a><?php
                        }
                        if (current_user_can('administrator')) { ?>
                            <div class="form-group">
                                <label class="form__label form-login__label" for="admin_block_user">
                                    <input type="checkbox" id="admin_block_user" class="form-check-input" data-userid="<?= $uID ?>" <?= ((int)get_user_meta($uID, 'admin_blocked', true) ? 'checked' : '') ?>>
                                    <?= __('Block User Profile', 'base') ?>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="form__label form-login__label" for="admin_payment_user">
                                    <input type="checkbox" id="admin_payment_user" class="form-check-input" data-userid="<?= $uID ?>" <?= ((int)get_user_meta($uID, 'admin_payment', true) ? 'checked' : '') ?>>
                                    <?= __('User Payment', 'base') ?>
                                </label>
                            </div>
                            <div class="form-group">
                                <p><?= __('StripeSesID', 'base').':'.get_user_meta($uID, 'payment_stripe_sesid', true) ?></p>
                                <p><?= __('PayPalSesID', 'base').':'.get_user_meta($uID, 'payment_paypal_sesid', true) ?></p>
                            </div><?php
                        } ?>
                    </div>
                </div>

            </div>
        </div>
    </section><?php
}


//===MY ACCOUNT VIEW===
function page_myaccview($uID = 0){
    //===CHECK PAYMENT===
    $checkUserCS = checkUserCS(true);
    if($checkUserCS){
        echo $checkUserCS;
        exit();
    }

    $uID          = (int)$uID;
    $userdata     = get_user_by('id', $uID);
    $current_user = get_current_user_id(); ?>

    <section class="container">
        <div class="row">
            <div class="col-md-12"><?php
                if(!$userdata){ ?>
                    <h3><?= __('User not exists!', 'base') ?></h3><?php
                }else{ ?>
                    <img src="<?= get_myavatar_url($uID, 'thumbnail_140x140') ?>" alt="Name" width="140" height="140">
                    <strong><?= $userdata->first_name ?></strong>
                    <img src="<?= (get_yn_user_fav($uID) ?verified_y :verified_n) ?>" alt="favorite"><?php
                    if( get_user_meta($uID, 'usr_isee', true) ){ ?>
                        <img src="<?= get_template_directory_uri() ?>/img/icon-verified.svg" alt="verified"><?php
                        echo '<div>'.__("This profile is verified", "w51p").'</div>';
                    }else{ ?>
                        <img src="<?= get_template_directory_uri() ?>/img/icon-verified-no.svg" alt="noverified"><?php
                        echo '<div>'.__("This profile is not verified", "w51p").'</div>';
                    }

                    if( get_current_user_id() != $uID || current_user_can('administrator') ){
                        echo '<div><a href="'.get_page_link(get_field('sopt_messages_pid', 'option')).'" class="btn btn-primary wd_pmsg_initlist" data-username="'.$userdata->first_name.'">'.__("Start messaging", "w51p").'</a></div>';
                    }
                    if( get_current_user_id() == $uID || current_user_can('administrator') ){
                        echo '<div><a href="'.get_page_link(get_field('sopt_myprofile_edit_pid', 'option')).'/?user='.$uID.'" class="btn btn-primary dc_myacc_edit" data-userid="'.$uID.'">'.__("Edit account information", "w51p").'</a></div>';
                    } ?>

                    
                    <h2><?= __('General Information', 'base') ?></h2>
                    <?= __('About myself:', 'base') ?><br>
                    <?= get_user_meta($uID, 'usr_bio', true) ?>
                    <div>
                        <?= __('On the website since:', 'base') ?>
                        <?= date(get_option('date_format'), strtotime($userdata->user_registered)) ?>
                    </div>
                    <div>
                        <?= __('I’m originally from:', 'base') ?>
                        <?= get_user_meta($uID, 'usr_info_1', true) ?>
                    </div>
                    <div>
                        <?= __('I\'ve Also Lived In:', 'base') ?>
                        <?= get_user_meta($uID, 'usr_info_2', true) ?>
                    </div>

                    <div>
                        <?= __('My Line of Work:', 'base') ?>
                        <?= get_user_meta($uID, 'usr_info_3', true) ?><?php
                        $fan   = [];
                        $fan[] = get_user_meta($uID, 'usr_test_1_1', true);
                        $fan[] = get_user_meta($uID, 'usr_test_1_2', true);
                        $fan[] = get_user_meta($uID, 'usr_test_1_3', true);
                        $fan[] = get_user_meta($uID, 'usr_test_1_4', true);
                        $fan[] = get_user_meta($uID, 'usr_test_1_5', true);
                        $fan[] = get_user_meta($uID, 'usr_test_1_6', true);
                        if($fan){
                            echo'<strong>'.__('I\'m a big fan of:', 'base').'</strong><span class="question-list">';
                            foreach($fan as $fani){
                                if($fani){
                                    echo $fani.'<span>,</span>&nbsp;';
                                }
                            }
                            echo'</span>';
                        } ?>
                    </div>

                    <h2><?= __('Questions about myself', 'base') ?>:</h2>
                    <div>
                        <?= __('If time and money were no object, where\'s one place you\'d travel to?', 'base') ?>
                        <?= get_user_meta($uID, 'usr_test_2_1', true) ?>
                    </div>
                    <div>
                        <?= __('The next big thing I\'m going to tackle is...', 'base') ?>
                        <?= get_user_meta($uID, 'usr_test_2_2', true) ?>
                    </div>
                    <div>
                        <?= __('I\'m getting better at...', 'base') ?>
                        <?= get_user_meta($uID, 'usr_test_2_3', true) ?>
                    </div>
                    <div>
                        <?= __('My advice for someone 10 years younger would be...', 'base') ?>
                        <?= get_user_meta($uID, 'usr_test_2_4', true) ?>
                    </div>
                    <div>
                        <?= __('What\'s something people would be surprised to know about you?', 'base') ?>
                        <?= get_user_meta($uID, 'usr_test_2_5', true) ?>
                    </div>


                    <h2><?= __('Social media links', 'base') ?></h2><?php
                    $usr_fb   = get_user_meta($uID, 'usr_fb',   true);
                    $usr_lin  = get_user_meta($uID, 'usr_lin',  true);
                    $usr_twit = get_user_meta($uID, 'usr_twit', true);
                    $usr_inst = get_user_meta($uID, 'usr_inst', true);
                    if($usr_fb){ ?>
                        <div class="profile__info-row">
                            <strong><?= __('Facebook', 'base') ?></strong>
                            <a href="<?= $usr_fb ?>" rel="nofollow" target="_blank"><span><?= $usr_fb ?></span></a>
                        </div><?php
                    }
                    if($usr_lin){ ?>
                        <div class="profile__info-row">
                            <strong><?= __('Linkedin', 'base') ?></strong>
                            <a href="<?= $usr_lin ?>" rel="nofollow" target="_blank"><span><?= $usr_lin ?></span></a>
                        </div><?php
                    }
                    if($usr_twit){ ?>
                        <div class="profile__info-row">
                            <strong><?= __('Twitter', 'base') ?></strong>
                            <a href="<?= $usr_twit ?>" rel="nofollow" target="_blank"><span><?= $usr_twit ?></span></a>
                        </div><?php
                    }
                    if($usr_inst){ ?>
                        <div class="profile__info-row">
                            <strong><?= __('Instagramm', 'base') ?></strong>
                            <a href="<?= $usr_inst ?>" rel="nofollow" target="_blank"><span><?= $usr_inst ?></span></a>
                        </div><?php
                    }
                    
                    if( get_current_user_id() != $uID || current_user_can('administrator') ){ ?>
                        <label for="report-met">
                            <input type="checkbox" id="report-met"            class="dc_frend_meet"      data-userid="<?= $uID ?>" <?= (get_user_meta($uID, 'usr_imeet', true) ? 'checked' : '') ?>>
                            <?= __('I met her in person', 'base') ?>
                        </label>
                        <div class="form__input-holder">
                            <label for="report-image">
                                <input type="checkbox" id="report-image"      class="dc_frend_see"       data-userid="<?= $uID ?>" <?= (get_user_meta($uID, 'usr_isee', true) ? 'checked' : '') ?>>
                                <?= __('I verify that her profile image is a true likeness', 'base') ?>
                            </label>
                        </div>
                        <div class="form__input-holder">
                            <label for="report-messagging"><?php
                                $blockedUsersArr = (array)unserialize(get_user_meta($uID, 'usr_blocked_pmsg', true)); ?>
                                <input type="checkbox" id="report-messagging" class="dc_frend_blockpmsg" data-usrid="<?= $uID ?>"  data-myid="<?= $current_user ?>" <?= (array_key_exists($current_user, $blockedUsersArr) ? 'checked' : '') ?>>
                                <?= __('Block messaging', 'base') ?>
                            </label>
                        </div>
                        <button class="btn btn-danger" type="submit" id="complain_action"><?= __('Report profile', 'base') ?></button><?php
                    }
                } ?>
            </div>
        </div>
    </section><?php
}


function get_myavatar_url($uID = 0, $size = 'thumbnail_48x48'){
    $img = (int)get_user_meta($uID, 'usr_ava', true);
    $img = wp_get_attachment_image_src($img, $size);
    return (isset($img[0]) ? $img[0] : esc_url(get_template_directory_uri()) . '/img/noava.jpg');
}


function getHelloUser($userID){
    $res            = '';
    $priced         = (int)get_field('sopt_paymentd_s',        'option');
    $pricef         = (int)get_field('sopt_payment_s',         'option');
    $sopt_usrc_sail = (int)get_field('sopt_usrc_sail',         'option');
    $sopt_usrc_full = (int)get_field('sopt_usrc_full',         'option');
    $free_min       = (int)get_field('sopt_ntf_int_block_min', 'option');
    $countUser      = (int)get_user_meta($userID, 'payment_ucount', true);
    $countUser++;
    
    if( $countUser <= $sopt_usrc_sail ){
        $res   = get_field('sopt_ntf_txt_free', 'option');
        $price = 0;
        update_user_meta($userID, 'admin_payment', 1);
    }elseif( $countUser <= $sopt_usrc_full ){
        $price = $priced;
        $res   = get_field('sopt_ntf_txt_discount', 'option').'
        <div class="popup-payment__info">
          <div class="popup-payment__row">
            <span>'.__('Subtotal:', 'base').'</span>
            <span>$'.$pricef.'.00</span>
          </div>
          <div class="popup-payment__row">
            <span>'.__('Discount:', 'base').'</span>
            <span>-$'.abs($pricef - $priced).'</span>
          </div>
          <div class="popup-payment__row">
            <strong>'.__('Total:', 'base').'</strong>
            <strong>$'.$price.'.00</strong>
          </div>
        </div>
        <button id="GoToPaymentModal" class="btn btn--accent">'.__('Proceed to checkout', 'base').'</button>';
    }else{
        $price = $pricef;
        $res   = get_field('sopt_ntf_txt_full_price', 'option').(int)get_field('sopt_paymentd_s', 'option').'
        <div class="popup-payment__info">
          <div class="popup-payment__row">
            <span>'.__('Subtotal:', 'base').'</span>
            <span>$'.$price.'.00</span>
          </div>
          <div class="popup-payment__row">
            <strong>'.__('Total:', 'base').'</strong>
            <strong>$'.$price.'.00</strong>
          </div>
        </div>
        <button id="GoToPaymentModal" class="btn btn--accent">'.__('Proceed to checkout', 'base').'</button>';
    }

    $res = str_replace('[x-user]',            $countUser,      $res);
    $res = str_replace('[x-price]',           $price,          $res);
    $res = str_replace('[x-count-user-free]', $sopt_usrc_sail, $res);
    $res = str_replace('[x-count-user-full]', $sopt_usrc_full, $res);
    $res = str_replace('[x-mim-free]',        $free_min,       $res);
    
    return $res;
}


//===GETTING PRICE FOR REGISTER===
function getPriceRegister($userID=0){
    $userID  = (int)$userID;
    $userArr = getCountUserArea($userID);
    if ($userArr < (int)get_field('sopt_usrc_sail', 'option')) {
        return 0;
    } elseif ($userArr < (int)get_field('sopt_usrc_full', 'option')) {
        return (int)get_field('sopt_paymentd_s', 'option');
    } else {
        return (int)get_field('sopt_payment_s', 'option');
    }
}


//===CHANGE PASSWORD from my account===
add_action('wp_ajax_chpass',        'chpass');
//add_action('wp_ajax_nopriv_chpass', 'chpass');
function chpass(){
    $err = '';
    $_POST['usr_old_pass']    = $_POST['usr_old_pass'] ?? '';
    $_POST['usr_ch_pass']     = $_POST['usr_ch_pass']  ?? '';
    $_POST['usr_ch_pass_uid'] = (int)$_POST['usr_ch_pass_uid'];
    
    //===only admin can edit other account===
    if(current_user_can('administrator') && $_POST['usr_ch_pass_uid']){
        $user_id = $_POST['usr_ch_pass_uid'];
    }else{
        $user_id = get_current_user_id();
    }

    $user = get_user_by('id', $user_id);
    if(!wp_check_password($_POST['usr_old_pass'], $user->data->user_pass, $user->ID) ){
        $err = 'Old Password not correct.';
    }
    if( strpos($_POST['usr_ch_pass'], ' ') !== false ){
        $err = 'Password cannot contain a space.';
    }
    if(strlen($_POST['usr_ch_pass'])  < 3){
        $err = 'Please enter your "Password".';
    }

    if($err==''){
        wp_set_password( $_POST['usr_ch_pass'], $user_id );
        wp_set_current_user( $user_id );
        wp_set_auth_cookie( $user_id );
        $user = get_user_by( 'id', $user_id ); 
        do_action( 'wp_login', $user->user_login );
        $err = __('Password changed!', 'base');
    }
    die($err);
}


//===CREATE USER===
add_action('wp_ajax_regusr',        'regusr');
add_action('wp_ajax_nopriv_regusr', 'regusr');
function regusr(){
    $err = '';
    if( is_user_logged_in() ){
        $err = __('You are already logged in', 'base');
    }else{
        $_POST['usr_register_usr_zip_loc']   = sanitize_text_field(substr($_POST['usr_register_usr_zip_loc'], 0, 64));
        $_POST['usr_register_usr_zip_lt']    = sanitize_text_field(substr($_POST['usr_register_usr_zip_lt'],  0, 64));
        $_POST['usr_register_usr_zip_ln']    = sanitize_text_field(substr($_POST['usr_register_usr_zip_ln'],  0, 64));
        $_POST['usr_register_usr_pass']      = substr($_POST['usr_register_usr_pass'], 0, 64);
        $_POST['usr_register_usr_fname']     = sanitize_text_field(substr($_POST['usr_register_usr_fname'],   0, 64));
        $_POST['usr_register_usr_lname']     = sanitize_text_field(substr($_POST['usr_register_usr_lname'],   0, 64));
        $_POST['usr_register_usr_email']     = sanitize_email(substr($_POST['usr_register_usr_email'],        0, 64));

        //if(strlen($_POST['usr_register_usr_zip_loc']) < 3){$err = __('Enter a more detailed address.', 'base');}
        //if(strlen($_POST['usr_register_usr_zip_lt'])  < 3){$err = __('Enter a more detailed address.', 'base');}
        //if(strlen($_POST['usr_register_usr_zip_ln'])  < 3){$err = __('Enter a more detailed address.', 'base');}
        if(strpos($_POST['usr_register_usr_pass'], ' ') !== false){$err = __('Password cannot contain a space.', 'base');}
        if(strlen($_POST['usr_register_usr_pass'])    < 3){$err = __('Please enter you "Password".',   'base');}
        if(strlen($_POST['usr_register_usr_fname'])   < 3){$err = __('Please enter you "Last Name".',  'base');}
        if(strlen($_POST['usr_register_usr_lname'])   < 3){$err = __('Please enter you "First Name".', 'base');}
        if(!is_email($_POST['usr_register_usr_email']) ){$err   = __('Please enter you "E-Mail".',     'base');}

        if($err==''){
            $user = get_user_by('email', $_POST['usr_register_usr_email']);
            if(!$user){
                $userdata = array(
                    'user_pass'     => $_POST['usr_register_usr_pass'],
                    'user_login'    => $_POST['usr_register_usr_email'],
                    'user_nicename' => '',
                    'user_email'    => $_POST['usr_register_usr_email'],
                    'display_name'  => '',
                    'nickname'      => '',
                    'first_name'    => $_POST['usr_register_usr_fname'],
                    'last_name'     => $_POST['usr_register_usr_lname'],
                    'role'          => s51m_user_role,
                );
                $user_id = wp_insert_user($userdata);
                if( is_integer($user_id) && $user_id>0 ){
                    $usersecret     = rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
                    
                    update_user_meta($user_id, 'usr_zip_loc',      $_POST['usr_register_usr_zip_loc']);
                    update_user_meta($user_id, 'usr_zip_lt',       $_POST['usr_register_usr_zip_lt']);
                    update_user_meta($user_id, 'usr_zip_ln',       $_POST['usr_register_usr_zip_ln']);
                    update_user_meta($user_id, 'usersecret',       $usersecret);
                    update_user_meta($user_id, 'usr_blocked_pmsg', serialize([]));
                    update_user_meta($user_id, 'new_pmsg',         serialize([]));
                    update_user_meta($user_id, 'usr_visible',      1);
                    update_user_meta($user_id, 'admin_blocked',    0);
                    update_user_meta($user_id, 'admin_payment',    0);
                    
                    $payment_price  = getPriceRegister($user_id);
                    $payment_ucount = getCountUserArea($user_id);

                    update_user_meta($user_id, 'payment_price',    $payment_price);
                    update_user_meta($user_id, 'payment_ucount',   $payment_ucount);

                    $recURL = esc_url(home_url()).'/?usersecretreg='.$usersecret;
                    $emssg = get_field('sopt_emls_confirmemail', 'option');
                    $emssg = str_replace('[fname]',   $_POST['usr_register_usr_fname'], $emssg);
                    $emssg = str_replace('[lname]',   $_POST['usr_register_usr_lname'], $emssg);
                    $emssg = str_replace('[email]',   $_POST['usr_register_usr_email'], $emssg);
                    $emssg = str_replace('[confurl]', $recURL, $emssg);
                    siteMail($_POST['usr_register_usr_email'], $emssg);
                    $err .= __('Thank you for signing up with Fifty-One Percent Club! Please check your email for a confirmation email to complete registration.', 'base');
                }else{
                    $err .= __('Internal error!', 'base');
                }
            }else{
                $err .= __('This E-Mail already exists.', 'base');
            }
        }
    }
    die($err);
}


//===AUTH USER===
add_action('wp_ajax_authusr',        'authusr');
add_action('wp_ajax_nopriv_authusr', 'authusr');
function authusr(){
    $_POST['usr_login_email'] = sanitize_email($_POST['usr_login_email']);
    $_POST['usr_login_pass']  = sanitize_text_field($_POST['usr_login_pass']);
    $err = '';

    if(strlen($_POST['usr_login_pass'])  < 3){$err = __('Please enter your "Password".', 'base');}
    if(!is_email($_POST['usr_login_email']) ){$err = __('Please enter your "E-Mail".',   'base');}

    if($err==''){
        //===check confirmed email===
        $user = get_user_by('email', $_POST['usr_login_email']);
        if($user){
            if( (int)get_user_meta($user->ID, 'usersecret', true) ){
                $err = __('This email is not verified!', 'base');
            }
            //if( (int)get_user_meta($user->ID, 'admin_blocked', true) ){
                //$err = get_field('sopt_ntf_txt_adminblockuser', 'option');
            //}
            //if( (int)get_user_meta($uID, 'admin_payment', true)==0 ){
                //$err = get_field('sopt_ntf_txt_block20min', 'option');
            //}
        }else{
            $err = __('This E-Mail does not exist on this site!', 'base');
        }

        if($err==''){
            $creds = array(
                    'user_login'    => $_POST['usr_login_email'],
                    'user_password' => $_POST['usr_login_pass'],
                    'remember'      => true
                );
            $user = wp_signon($creds, false);
            if(!is_wp_error($user)){
                update_user_meta($user->ID, 'login_count', ((int)get_user_meta($user->ID, 'login_count', true)+1) );
            }else{
                $err = __('Authorization error.', 'base');
            }
        }
    }
    echo json_encode(['err'=>$err]);
    die();
}



//===RESTORE PASSWORD===
add_action('wp_ajax_restpass',        'restpass');
add_action('wp_ajax_nopriv_restpass', 'restpass');
function restpass(){
    $_POST['usr_restore_email'] = sanitize_email($_POST['usr_restore_email']);
    $err = '';

    if(!is_email($_POST['usr_restore_email']) ){$err = 'Please enter valid E-Mail.';}

    if($err==''){
        $user = get_user_by('email', $_POST['usr_restore_email']);
        if($user){
            if( strlen(get_user_meta($user->ID, 'usersecret', true))==0 ){
                $usersecret = rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
                update_user_meta($user->ID, 'usersecretrestore', $usersecret);
                $restURL = get_field('sopt_recovery_url', 'option').'/?usersecretrestore='.$usersecret;
                $emssg   = get_field('sopt_emls_restorepass', 'option');
                $emssg   = str_replace('[fname]',     $user->first_name, $emssg);
                $emssg   = str_replace('[lname]',     $user->last_name, $emssg);
                $emssg   = str_replace('[email]',     $_POST['usr_restore_email'], $emssg);
                $emssg   = str_replace('[restorurl]', $restURL, $emssg);
                siteMail($_POST['usr_restore_email'], $emssg);
                $err = __('Go to you E-Mail box.', 'base');
            }else{
                $err .= __('Thank you for signing up with Fifty-One Percent Club! Please check your email for a confirmation email to complete registration.', 'base');
            }
        }else{
            $err = __('This E-Mail does not exist on this site!', 'base');
        }
    }
    echo $err;
    die();
}



//===complaine to user ACTION===
add_action('wp_ajax_complaineuser',        'complaineuser');
//add_action('wp_ajax_nopriv_complaineuser', 'complaineuser');
function complaineuser(){
    $res = '';
    $_POST['message']  = substr(sanitize_text_field($_POST['message']), 0, 300);
    $_POST['tuUserID'] = (int)$_POST['tuUserID'];

    if( strlen($_POST['message'])<6 ){
        $res .= __('Please enter your Message.', 'base');
    }

    $userF  = get_user_by('id', get_current_user_id());
    $userTU = get_user_by('id', $_POST['tuUserID']);
    $userT  = get_user_by('id', get_field('sopt_51p_admin_id', 'option'));

    if( $userF && $userT && $userTU ){
        setLog($userT->ID, 'Complain to user! From:'.$userF->user_email.' to:'.$userTU->user_email.' '.$_POST['message']);
        $res .= __('Your message has been sent', 'base');
    }else{
        $res .= __('Error!', 'base');
    }

    echo $res;
    die();
}


//===CHYEAR to user ACTION===
add_action('wp_ajax_chyearuser',        'chyearuser');
//add_action('wp_ajax_nopriv_chyearuser', 'chyearuser');
function chyearuser(){
    $res = '';
    $_POST['message']  = substr(sanitize_text_field($_POST['message']), 0, 300);

    if( strlen($_POST['message'])<6 ){
        $res .= __('Please enter your Message', 'base');
    }

    $userF  = get_user_by('id', get_current_user_id());
    $userT  = get_user_by('id', (int)get_field('sopt_51p_admin_id', 'option'));
    if( $userF && $userT ){
        setLog($userT->ID, 'Change user age! From:'.$userF->user_email.' '.$_POST['message']);
        $res .= __('Your message has been sent', 'base');
    }else{
        $res .= __('Error!', 'base');
    }

    echo $res;
    die();
}


//===Y/N PAYMENT USER===
add_action('wp_ajax_paymentuser',        'paymentuser');
//add_action('wp_ajax_nopriv_paymentuser', 'paymentuser');
function paymentuser(){
    $_POST['usrid'] = $_POST['usrid'] ?? 0;
    $_POST['usrid'] = (int)$_POST['usrid'];
    //===only admin can edit payment to account===
    if(current_user_can('administrator') && $_POST['usrid']){
        $res = (int)get_user_meta($_POST['usrid'], 'admin_payment', true);
        if($res==0){
            $res = 1;
            update_user_meta($_POST['usrid'], 'admin_payment', $res);
        }else{
            $res = 0;
            update_user_meta($_POST['usrid'], 'admin_payment', $res);
        }
    }
    echo $res;
    die();
}


//===BLOCK/UNBLOCK USER===
add_action('wp_ajax_blockuser',        'blockuser');
//add_action('wp_ajax_nopriv_blockuser', 'blockuser');
function blockuser(){
    $_POST['usrid'] = $_POST['usrid'] ?? 0;
    $_POST['usrid'] = (int)$_POST['usrid'];
    //===only admin can edit other account===
    if(current_user_can('administrator') && $_POST['usrid']){
        $res = (int)get_user_meta($_POST['usrid'], 'admin_blocked', true);
        if($res==0){
            $res = 1;
            update_user_meta($_POST['usrid'], 'admin_blocked', $res);
        }else{
            $res = 0;
            update_user_meta($_POST['usrid'], 'admin_blocked', $res);
        }
    }
    echo $res;
    die();
}


//===I met her in person===
add_action('wp_ajax_imeetuser',        'imeetuser');
//add_action('wp_ajax_nopriv_imeetuser', 'imeetuser');
function imeetuser(){
    $userid = $_POST['userid'] ?? (int)$_POST['userid'] ?? 0;
    $res = (int)get_user_meta($_POST['userid'], 'usr_imeet', true);
    if($res){
        update_user_meta($_POST['userid'], 'usr_imeet', 0);
    }else{
        update_user_meta($_POST['userid'], 'usr_imeet', 1);
    }
    die();
}


//===I verify that her profile image is a true likeness===
add_action('wp_ajax_iseeuser',        'iseeuser');
//add_action('wp_ajax_nopriv_iseeuser', 'iseeuser');
function iseeuser(){
    $userid = $_POST['userid'] ?? (int)$_POST['userid'] ?? 0;
    $res = (int)get_user_meta($_POST['userid'], 'usr_isee', true);
    if($res){
        update_user_meta($_POST['userid'], 'usr_isee', 0);
    }else{
        update_user_meta($_POST['userid'], 'usr_isee', 1);
    }
    die();
}


//===UPDATE USER AVA===
add_action('wp_ajax_updatemyaccava',        'updatemyaccava');
//add_action('wp_ajax_nopriv_updatemyaccava', 'updatemyaccava');
function updatemyaccava(){
    $err = ['txt'=>'', 'ava'=>'', 'avab'=>''];
    
    //===only admin can edit other account===
    if(current_user_can('administrator') && $_POST['usr_id']){
        $user_id = $_POST['usr_id'];
    }else{
        $user_id = get_current_user_id();
    }

    //===add avatar===
    if( isset($_FILES['usr_ava']) ){
        if(isset($_FILES['usr_ava']) && isset($_POST['fileup_nonce']) && count($_FILES['usr_ava'])>0 && $_FILES['usr_ava']['tmp_name'] && wp_verify_nonce($_POST['fileup_nonce'], 'usr_ava')){
            $file = $_FILES['usr_ava'];
            require_once( ABSPATH . 'wp-admin/includes/admin.php' );
            $file_return = wp_handle_upload( $file, array('test_form' => false ) );
            if( isset( $file_return['error'] ) || isset( $file_return['upload_error_handler'] ) ) {
                $err['txt'] .= __('Error uploading avatar!', 'base');
            }else{
                $filename = $file_return['file'];
                $attachment = array(
                    'post_mime_type' => $file_return['type'],
                    'post_title'     => htmlspecialchars(basename($filename)),
                    'post_content'   => '',
                    'post_status'    => 'inherit',
                    'guid'           => $file_return['url'],
                );
                $attachment_id = wp_insert_attachment( $attachment, $file_return['url'] );
                require_once(ABSPATH . 'wp-admin/includes/image.php');
                $attachment_data = wp_generate_attachment_metadata( $attachment_id, $filename );
                wp_update_attachment_metadata( $attachment_id, $attachment_data );
                update_user_meta($user_id, 'usr_ava', $attachment_id);
                $err['txt']  .= __('Avatar Uploaded!', 'base');
                $err['ava']  = get_myavatar_url(get_current_user_id(), 'thumbnail_64x64');
                $err['avab'] = get_myavatar_url(get_current_user_id(), 'thumbnail_64x64');
            }
        }
    }
    
    echo json_encode($err);
    die();
}


//===UPDATE USER===
add_action('wp_ajax_updatemyaccusr',        'updatemyaccusr');
//add_action('wp_ajax_nopriv_updatemyaccusr', 'updatemyaccusr');
function updatemyaccusr(){
    $err = ['txt'=>''];
    $_POST['usr_id']             = (int)$_POST['usr_id'];
    $_POST['usr_zip_loc']        = sanitize_text_field($_POST['usr_zip_loc']);
    $_POST['usr_zip_lt']         = sanitize_text_field($_POST['usr_zip_lt']);
    $_POST['usr_zip_ln']         = sanitize_text_field($_POST['usr_zip_ln']);
    $_POST['usr_fname']          = sanitize_text_field($_POST['usr_fname']);
    $_POST['usr_lname']          = sanitize_text_field($_POST['usr_lname']);
    $_POST['usr_visible']        = ($_POST['usr_visible']=='on' ?1 :0);

    //if(strlen($_POST['usr_zip_loc'])  < 3){$err['txt'] = 'Enter a more detailed address.';}
    //if(strlen($_POST['usr_zip_lt'])   < 3){$err['txt'] = 'Enter a more detailed address.';}
    //if(strlen($_POST['usr_zip_ln'])   < 3){$err['txt'] = 'Enter a more detailed address.';}
    if(strlen($_POST['usr_fname'])    < 3){$err['txt'] = 'Please enter your "First Name".';}
    if(strlen($_POST['usr_lname'])    < 3){$err['txt'] = 'Please enter your "Last Name".';}

    //===only admin can edit other account===
    if(current_user_can('administrator') && $_POST['usr_id']){
        $user_id = $_POST['usr_id'];
    }else{
        $user_id = get_current_user_id();
    }

    if($err['txt']==''){
        wp_update_user( [
            'ID'         => $user_id,
            'first_name' => $_POST['usr_fname'],
            'last_name'  => $_POST['usr_lname'],
        ] );
        if( current_user_can('administrator') ){
            
        }
        update_user_meta($user_id, 'usr_zip_loc', $_POST['usr_zip_loc']);
        update_user_meta($user_id, 'usr_zip_lt',  $_POST['usr_zip_lt']);
        update_user_meta($user_id, 'usr_zip_ln',  $_POST['usr_zip_ln']);
        update_user_meta($user_id, 'usr_visible', $_POST['usr_visible']);

        //===NOTIFY TO ADMIN===
        $userF = get_user_by('id', get_field('sopt_51p_admin_id', 'option'));
        $userT = get_user_by('id', $user_id);
        if( $userF && $userT ){
            setLog(get_field('sopt_51p_admin_id', 'option'), 'Account updated: '.$userT->user_email);
        }

        $err['txt'] .= '<br/>User Updated!';
    }
    echo json_encode($err);
    die();
}


//===SET CONFIRMATION from my account===
add_action('wp_ajax_chconfirmation',        'chconfirmation');
//add_action('wp_ajax_nopriv_chconfirmation', 'chconfirmation');
function chconfirmation(){
    $_POST['usr_cf_1']   = $_POST['usr_cf_1'] ?? (int)$_POST['usr_cf_1'] ?? 0;
    $_POST['usr_cf_2']   = $_POST['usr_cf_2'] ?? (int)$_POST['usr_cf_2'] ?? 0;
    $_POST['usr_cf_3']   = $_POST['usr_cf_3'] ?? (int)$_POST['usr_cf_3'] ?? 0;
    $_POST['usr_cf_4']   = $_POST['usr_cf_4'] ?? (int)$_POST['usr_cf_4'] ?? 0;
    $_POST['usr_cf_uid'] = (int)$_POST['usr_cf_uid'];
    $err = '';

    //===only admin can edit other account===
    if(current_user_can('administrator') && $_POST['usr_cf_uid']){
        $user_id = $_POST['usr_cf_uid'];
    }else{
        $user_id = get_current_user_id();
    }

    update_user_meta( $user_id, 'usr_cf_1', $_POST['usr_cf_1']);
    update_user_meta( $user_id, 'usr_cf_2', $_POST['usr_cf_2']);
    update_user_meta( $user_id, 'usr_cf_3', $_POST['usr_cf_3']);
    update_user_meta( $user_id, 'usr_cf_4', $_POST['usr_cf_4']);
    $err = __('Confirmation settings changed!', 'base');
    
    echo json_encode(['err'=>$err]);
    die();
}


//===edit my account===
add_action('wp_ajax_myaccedit',        'myaccedit');
//add_action('wp_ajax_nopriv_myaccedit', 'myaccedit');
function myaccedit(){
    $_POST['userid'] = $_POST['userid'] ?? '';
    $_POST['userid'] = (int)$_POST['userid'];

    page_myaccedit($_POST['userid']);
    die();
}


//===view my account===
add_action('wp_ajax_myaccview',        'myaccview');
//add_action('wp_ajax_nopriv_myaccview', 'myaccview');
function myaccview(){
    $_POST['userid'] = $_POST['userid']   ?? '';
    $_POST['userid'] = (int)$_POST['userid'];

    page_myaccview($_POST['userid']);
    die();
}


//===LOGUUT user===
function checkUserCS($ajax=false){
    $res = '';

    if( is_user_logged_in() &&
        !current_user_can('administrator') &&
        ( (int)get_user_meta(get_current_user_id(), 'admin_payment', true)==0 || (int)get_user_meta(get_current_user_id(), 'admin_hello', true)==0 ) &&
        !in_array(get_queried_object_id(), [get_field('sopt_logout_pid', 'option')])
    ){
        //===SAY HELLO after register===
        if( (int)get_user_meta(get_current_user_id(), 'admin_hello', true)==0 ){
            update_user_meta(get_current_user_id(), 'admin_hello', 1);
            $_GET['AdminHello'] = 1;
        }

        //===ADMIN block user===
        if( (int)get_user_meta(get_current_user_id(), 'admin_blocked', true)==1 ){
            wp_logout();
            
            if($ajax){
                return '<script>top.location.href="'.home_url().'/?AdminBlockUser";</script>';
            }else{
                wp_redirect( home_url().'/?AdminBlockUser' );
                exit();
            }
        }

        //===goto payment===
        if( !isset($_GET['AdminHello']) && !isset($_GET['openPayModal']) ){
            $current_user = wp_get_current_user();
            $tsdt         = get_field('sopt_ntf_int_block_min', 'option')*60;
            $tsreg        = strtotime($current_user->user_registered);
            if( (time()-$tsreg) > $tsdt ){
                if($ajax){
                    return '<script>top.location.href="'.home_url().'/?openPayModal";</script>';
                }else{
                    wp_redirect( home_url().'/?openPayModal' );
                    exit();
                }
            }
        }

    }
}
add_action( 'template_redirect', 'checkUserCS' );


//===SET NEW PASSWORD===
function wpdocs_f11(){
    if(isset($_POST['usr_prestset_secret'])){
        $users = get_users([
            'blog_id'        => $GLOBALS['blog_id'],
            'role'           => s51m_user_role,
            'role__in'       => [],
            'role__not_in'   => [],
            'meta_key'       => '',
            'meta_value'     => '',
            'meta_compare'   => '',
            'meta_query'     => [],
            'include'        => [],
            'exclude'        => [],
            'orderby'        => '',
            'order'          => 'DESC',
            'offset'         => '',
            'search'         => '',
            'search_columns' => [],
            'number'         => -1,
            'paged'          => 1,
            'count_total'    => false,
            'fields'         => 'ID',
            'who'            => '',
            'has_published_posts' => null,
            'date_query'     => [],
            'meta_query'     =>[
                                    [
                                        'relation' => 'AND',
                                        [
                                                'key'     => 'usersecretrestore',
                                                'value'   => sanitize_text_field($_POST['usr_prestset_secret']),
                                                'compare' => '=',
                                                'type'    => 'numeric',
                                        ],
                                    ]
                                ]
        ]);
        foreach( $users as  $userID ) {
            wp_set_password( $_POST['usr_prestset_pass'], $userID );
            clean_user_cache( $userID );
            update_user_meta( $userID, 'usersecretrestore', '' );
            wp_redirect( home_url().'/?RestorePass' );
            exit();
        }
    }
}
add_action( 'after_setup_theme', 'wpdocs_f11' );


//===CONFIRM EMAIL===
if(isset($_GET['usersecretreg'])){
    $args = array(  
            'meta_key'   => 'usersecret',
            'meta_value' => sanitize_text_field($_GET['usersecretreg']),
        );
    $users = get_users( $args );
    $_GET['usersecretreg'] = 'You confirmation code not active!';
    foreach ( $users as  $my_users ) {
        $uID = $my_users->ID;
        update_user_meta($uID, 'usersecret', '');
        $_GET['usersecretreg'] = 'Your E-Mail confirmed!<br/>Please Login.';
    }
}






?>