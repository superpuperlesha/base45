<?php
/**
 ** Template Main List
 **/

$per_page;                 /* posts per page */
$dscc_filter_city;         /* ID city */
$search_by;                /* search by 0-city 1-myradius */
$dscc_filter_ts_start_dfs; /* start timestamp */
$dscc_filter_paging;       /* paging int */
$dscc_filter_type_show;    /* filter type 0-city+radius 1-city 2-radius */
$dscc_filter_allpostcount; /* All finded posts count */
$dscc_filter_allpagecount; /* All finded page count */
$geo_crd_accuracy;         /* Browser geolocation Accuracy */
$geo_crd_longitude;        /* Browser geolocation Longitude */
$geo_crd_latitude;         /* Browser geolocation Latitude */
$dscc_filter_radius;       /* Browser geolocation Radius */

$res_template .= '<!--Left list-->
				  <div class="dscc_filter_res1">
	                   	<h3>' .
			                 ( $search_by == 0 ? esc_html( __( 'City search result', 'rent_angry_cat' ) ) : '' ) .
			                 ( $search_by == 1 ? esc_html( __( 'My radius result', 'rent_angry_cat' ) ) : '' ) .
			                 '(' . (int) $dscc_filter_allpostcount . ')
                    	</h3>';

$dc_search_map_arr = [];
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		$thumbnail           = wp_get_attachment_image_src( get_post_thumbnail_id(), 'thumbnail_250x200' );
		$thumbnail           = ( isset( $thumbnail[0] ) ? $thumbnail[0] : wc_placeholder_img_src( 'woocommerce_thumbnail' ) );
		$thumb_alt           = esc_attr( get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true ) );
		$thumb_alt           = ( $thumb_alt ? $thumb_alt : esc_attr( get_the_title() ) );
		$dscc_latitude       = \Rent_Angry_Cat_ns\Rent_Angry_Cat::getCatLT( get_the_id() );
		$dscc_longitude      = \Rent_Angry_Cat_ns\Rent_Angry_Cat::getCatLN( get_the_id() );
		$dc_search_map_arr[] = [
			'lat'   => (float) $dscc_latitude,
			'lng'   => (float) $dscc_longitude,
			'title' => get_the_title()
		];
		$item_gall_id        = 'rac_gall_item_' . get_the_id();
		$item_video_id       = 'rac_video_item_' . get_the_id();
		$thumb_list          = array();
		$video_list          = '';
		$product             = wc_get_product( get_the_id() );

		$gallery_list = $product->get_gallery_image_ids();
		foreach ( $gallery_list as $attachment_id ) {
			$thumb_big    = wp_get_attachment_image_src( $attachment_id, 'thumbnail_1920x1080' );
			$thumb_sml    = wp_get_attachment_image_src( $attachment_id, 'thumbnail_250x200' );
			$thumb_list[] = [
				"src"     => $thumb_big[0],
				"thumb"   => $thumb_sml[0],
				"subHtml" => get_the_title(),
			];
		}

		$video_list         = get_post_meta( get_the_id(), 'dscc_video', true );
		$video_list_content = '';
		if ( $video_list ) {
			$video_list = explode( ';', $video_list );
			foreach ( $video_list as $video_list_item ) {
				$video_list_content .= '<a href="' . esc_attr( $video_list_item ) . '"></a>';
			}
		}

		$res_template .= '<!--Item-->
						  <div class="dscc_item_box">
	                            <div class="dscc_itemcar">
	                               <h4>' . get_the_title() . '</h4>
	                               <img src           = "' . esc_url( $thumbnail ) . '"
	                                    width         = "250"
	                                    height        = "200"
	                                    alt           = "' . esc_attr( $thumb_alt ) . '"
	                               >
	                               
	                               <a  href          = "#uID"
	                                   class         = "dscc_itemcar_sliderclicker"
	                                   id            = "' . esc_attr( $item_gall_id ) . '"
	                                   data-thumbarr = \'' . json_encode( $thumb_list ) . '\'
	                               >
	                                    ' . __( 'Photos', 'rent_angry_cat' ) . '
	                               </a>
	                               
	                               ' . ( $video_list_content ? ' <span class="dscc_itemcar_videoclicker" id="' . esc_attr( $item_video_id ) . '">
						                                             ' . $video_list_content . '
						                                              ' . esc_html( __( 'Video', 'rent_angry_cat' ) ) . '
						                             			 </span>'
															 : '' ) . '
	                            </div>
	                            <div id="car_' . get_the_id() . '" class="dscc_itemcartable">
	                               ' . \Rent_Angry_Cat_ns\Rent_Angry_Cat::dscc_gettemplate_ttable( get_the_id(), $dscc_filter_ts_start_dfs ) . '
	                            </div>
	                      </div>';
	}
} else {
	$res_template .= __( 'Not found.', 'rent_angry_cat' );
}

$res_template .= '<!--Bottom pagination-->
					  <div class="dscc_filter_page_box">
		                 <div id="dscc_filter_page_prev" class="dscc_filter_page_nextprevnoactive dscc_filter_page_prev">&LT;</div>
		                 <div id="dscc_filter_page_oll"  class="dscc_filter_page_oll" data-allpagecount="' . esc_attr( $dscc_filter_allpagecount ) . '" data-perpage="' . esc_attr( $per_page ) . '">' . esc_html( $dscc_filter_paging . '/' . $dscc_filter_allpagecount ) . '</div>
		                 <div id="dscc_filter_page_next" class="dscc_filter_page_nextprevnoactive dscc_filter_page_next">&GT;</div>
		              </div>
				 </div>
				 
				 <!--Right map-->
				 <div id="dscc_filter_res2" class="dscc_filter_res2">
	                 <div id="dscc_filter_map" data-dc_search_map_arr=\'' . esc_attr( json_encode( $dc_search_map_arr ) ) . '\' data-dc_search_map_myloc="' . esc_attr( __( 'My location!', 'rent_angry_cat' ) ) . '"></div>
	                 <div id="dscc_filter_map_title">' .
                 ( $geo_crd_accuracy && $geo_crd_latitude && $geo_crd_longitude && $search_by === 1
	                 ? __( 'radius:', 'rent_angry_cat' ) . $dscc_filter_radius . 'kM; ' . __( 'lat:', 'rent_angry_cat' ) . $geo_crd_latitude . '; ' . __( 'lng:', 'rent_angry_cat' ) . $geo_crd_longitude . '; ' . __( 'accuracy:', 'rent_angry_cat' ) . $geo_crd_accuracy . ';'
	                 : ''
                 ) . '
	                 </div>
                 </div>';
?>