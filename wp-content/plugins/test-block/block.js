var el                = wp.element.createElement,
    registerBlockType = wp.blocks.registerBlockType,
    withSelect        = wp.data.withSelect;
	
registerBlockType( 'riad/latest-post', {
    title:    'xxx JS GuttBG Block',
    icon:     'megaphone',
    category: 'widgets',

    edit: withSelect( function(select){
		const posts = select('core').getEntityRecords('postType', 'post', {'per_page':1});
		return{posts};
    } )( function(props){
		const{posts} = props;
		
        if(typeof posts == null || posts == null || posts.length==0){
            return "No posts JS";
        }
		
		var post = posts[0];
		
		console.log( post );
        
		var className = post.className;
		var link      = post.link;
		var title     = '['+post.id+']:'+post.title.raw+' ';
        return el(
            'a',
            {className:className, href:link},
            title
        );
    } ),

    save: function() {
        // Rendering in PHP
        return null;
    },
} );

